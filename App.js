import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, View,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import store from './src/store/reducers/store';
// import screenow
import AuthenticationScreen from './src/components/screens/Authentication/Authentication';
import RegistrationScreen from './src/components/screens/Registration/Registration';
import MainScreen from './src/components/screens/Main/Main';
import ShareTournamentScreen from './src/components/screens/ShareTournament/ShareTournament';
import SideMenu from './src/components/screens/SideMenu/SideMenu';
import TournamentDescriptionScreen from './src/components/screens/TournamentDescription/TournamentDescription';
import ActiveGameScreen from './src/components/screens/ActiveGame/ActiveGame';
import GameHistoryScreen from './src/components/screens/GameHistory/GameHistory';
import AddResultModal from './src/components/screens/AddResultModal/AddResultModal';
import JoinTournamentModal from './src/components/screens/JoinTournamentModal/JoinTournamentModal';
import RankingScreen from './src/components/screens/Ranking/Ranking';
import TabGenerator from './src/components/screens/TabGenerator/TabGenerator';
import FilterModal from './src/components/screens/FilterModal/FilterModal';
// rejestracja screenow

console.disableYellowBox = true;

Navigation.registerComponentWithRedux(
  'games-app.AuthenticationScreen',
  () => AuthenticationScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.FindTournamentScreen',
  () => FindTournamentScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.ShareTournamentScreen',
  () => ShareTournamentScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.RegistrationScreen',
  () => RegistrationScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.MainScreen',
  () => MainScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.SideMenu',
  () => SideMenu,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.TournamentDescriptionScreen',
  () => TournamentDescriptionScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.ActiveGameScreen',
  () => ActiveGameScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.GameHistoryScreen',
  () => GameHistoryScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.AddResultModal',
  () => AddResultModal,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.JoinTournamentModal',
  () => JoinTournamentModal,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.RankingScreen',
  () => RankingScreen,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.TabGenerator',
  () => TabGenerator,
  Provider,
  store,
);

Navigation.registerComponentWithRedux(
  'games-app.FilterModal',
  () => FilterModal,
  Provider,
  store,
);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        id: 'mainStack',
        children: [{
          component: {
            name: 'games-app.AuthenticationScreen',
          },
        }],
      },
    },
  });
});

// const _retrieveData = async () => {
//   try {
//     const user = await AsyncStorage.getItem('user');
//     console.log(user);
//     if (user !== null) {
//       const { id, email, token } = JSON.parse(user);
//       this.setState({
//         id,
//         email,
//         token,
//       })
//       // We have data!!
//       // console.log(value);
//     }
//   } catch (error) {
//     return 'Error retrieving data'
//   }
// };
// _retrieveData();


// Navigation.events().registerAppLaunchedListener(() => {

//   Promise.all([
//     Icon.getImageSource("md-menu", 30),
//   ]).then(source => {
//     Navigation.setRoot({
//       root: {
//         sideMenu: {
//           //id: "sideMenu",
//           right: {
//             component: {
//               //width: 200,
//               id: "rightDrawer",
//               name: "games-app.SideMenu",

//             }
//           },
//           center: {
//             stack: {
//               id: "AppRoot",
//               children: [{
//                 component: {
//                   id: "App",
//                   name: "games-app.AuthenticationScreen",
//                   options: {
//                     topBar: {
//                       rightButtons: [
//                         {
//                           id: 'buttonOne',
//                           icon: source[0],
//                         }
//                       ],

//                     }
//                   }
//                 }
//               }]
//             }
//           }
//         }
//       }
//     })
//   })
// })

// Navigation.events().registerAppLaunchedListener(() => {
//   Navigation.setRoot({
//     root: {
//       sideMenu: {
//         id: "sideMenu",
//         right: {
//           component: {
//             id: "Drawer",
//             name: "games-app.SideMenu",

//           }
//         },
//         center: {
//           stack: {
//             id: "AppRoot",
//             children: [{
//               component: {
//                 id: "App",
//                 name: "games-app.AuthenticationScreen",
//                 options: {
//                   topBar: {
//                     rightButtons: [
//                       {
//                         id: 'buttonOne',
//                         icon: source[0],
//                       }
//                     ],

//                   }
//                 }
//               }
//             }]
//           }
//         }
//       }
//     }
//   })
// })


