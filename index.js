import React from 'react';
import { Root } from 'native-base';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/store/reducers/store';
import App from './App';

const RNRedux = () => (
    <Root>
        <Provider store={ store } >
            <App />
        </Provider>
    </Root>
);

AppRegistry.registerComponent('testapp', () => RNRedux);
