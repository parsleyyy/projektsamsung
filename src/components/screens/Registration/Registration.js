import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
    Container, Body, Content, Button,
    Card, CardItem, Text, Item, Input,
    Spinner, View,
} from 'native-base';

class RegistrationScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            passwordConfirmation: '',
            message: '',
        };
    }

    updateValue(text, field) {
        this.setState({
            [field]: text,
        })
    }

    registerHandler() {
        let data = {};
        const { email, password, passwordConfirmation, firstName, lastName } = this.state;

        if (password === passwordConfirmation && email && password && passwordConfirmation && firstName && lastName) {
            data.email = email;
            data.password = password;
            data.firstName = firstName;
            data.lastName = lastName;
            console.log(data);

            fetch('https://tournament--app.herokuapp.com/api/user/create', {
                method: 'POST',
                body: JSON.stringify(data),
                headers: new Headers({
                    'Content-Type': 'application/json',
                })
            }).then(response => response.json())
                .then(response => {

                    const { message, userId } = response;
                    this.setState({
                        message,
                    });
                    if (userId) {
                        Navigation.setRoot({
                            root: {
                                stack: {
                                    id: "mainStack",
                                    children: [{
                                        component: {
                                            name: "games-app.AuthenticationScreen"
                                        }
                                    }]
                                }
                            }
                        });
                    }

                }).catch(err => err)
        };
    };

    render() {
        return (
            <Container>
                <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
                    <Card>
                        <CardItem>
                            <Body style={{ justifyContent: 'center', }}>
                                <Text style={{ textAlign: 'center', marginBottom: 10, justifyContent: 'center', alignSelf: 'center', fontSize: 35, }}>UTWÓRZ KONTO</Text>
                                <Text note style={{ color: '#6666ff', textAlign: 'center', justifyContent: 'center', alignSelf: 'center', fontSize: 15, }}>Uzupełnij wymagane informacje</Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Item rounded>
                                    <Input
                                        style={{ marginLeft: 15, }}
                                        placeholder='First Name'
                                        onChangeText={(text) => this.updateValue(text, 'firstName')}
                                        value={this.state.firstName}
                                    />
                                </Item>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Item rounded>
                                    <Input
                                        style={{ marginLeft: 15, }}
                                        placeholder="Last Name"
                                        onChangeText={(text) => this.updateValue(text, 'lastName')}
                                        value={this.state.lastName}
                                    />
                                </Item>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Item rounded>
                                    <Input
                                        style={{ marginLeft: 15, }}
                                        placeholder="E-Mail Address"
                                        onChangeText={(text) => this.updateValue(text, 'email')}
                                        value={this.state.email}
                                    />
                                </Item>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Item rounded>
                                    <Input
                                        style={{ marginLeft: 15, }}
                                        placeholder="Password"
                                        onChangeText={(text) => this.updateValue(text, 'password')}
                                        secureTextEntry={true}
                                        value={this.state.password}
                                    />
                                </Item>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Item rounded>
                                    <Input
                                        style={{ marginLeft: 15, }}
                                        placeholder="Confirm Password"
                                        onChangeText={(text) => this.updateValue(text, 'passwordConfirmation')}
                                        secureTextEntry={true}
                                        value={this.state.passwordConfirmation}
                                    />
                                </Item>
                            </Body>
                        </CardItem>
                        <CardItem >
                            <Button
                                rounded
                                style={{ width: "100%", justifyContent: 'center' }}
                                onPress={() => this.registerHandler("games-app.AuthenticationScreen")}
                            >
                                <Text style={{ textAlign: "center", justifyContent: 'center', fontSize: 15, fontWeight: 'bold' }} >
                                    Zarejestruj się
                                    </Text>
                            </Button>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
            // <ImageBackground style={styles.imgBackground} resizeMode='cover' source={require('../../../images/wallpaperLogin.jpg')}>
            //     <View style={styles.container}>
            //         

            //        
            //         
            //         </TextInput>
            //         <TextInput
            //             style={{ height: 40, fontSize: 15, backgroundColor: "white", opacity: 0.2, color: "black", width: "80%", marginBottom: 15 }}
            //             placeholder="E-Mail Address"
            //             onChangeText={(text) => this.updateValue(text, 'email')}
            //             value={this.state.email}
            //         >
            //         </TextInput>
            //         <TextInput
            //             style={{ height: 40, fontSize: 15, backgroundColor: "white", opacity: 0.2, color: "black", width: "80%", marginBottom: 15 }}
            //             placeholder="Password"
            //             onChangeText={(text) => this.updateValue(text, 'password')}
            //             secureTextEntry={true}
            //             value={this.state.password}
            //         >
            //         </TextInput>
            //         <TextInput
            //             style={{ height: 40, fontSize: 15, backgroundColor: "white", opacity: 0.2, color: "black", width: "80%", marginBottom: 15 }}
            //             placeholder="Confirm Password"
            //             onChangeText={(text) => this.updateValue(text, 'passwordConfirmation')}
            //             secureTextEntry={true}
            //             value={this.state.passwordConfirmation}
            //         >
            //         </TextInput>

            //         <TouchableOpacity
            //             style={styles.buttonLogin}
            //             onPress={() => this.registerHandler("games-app.AuthenticationScreen")}>
            //             <Text> REGISTER </Text>
            //         </TouchableOpacity>
            //         {/*<Text style={{ color: "white", }}>{this.state.message == ("User created" || "") ? '' : 'UZytkownik już istnieje.'}</Text>*/}

            //     </View>
            // </ImageBackground >
        );
    }
}



// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         //justifyContent: "center",
//         alignItems: "center",
//     },
//     imgBackground: {
//         width: "100%",
//         height: "100%",
//         flex: 1,
//     },
//     hugeText: {
//         fontSize: 45,
//         color: "white",
//         fontFamily: "Arial",
//         fontWeight: "bold",
//         marginTop: 55,
//         //marginBottom: 5,

//     },
//     smallText: {
//         color: "white",
//         marginBottom: 50,
//         fontSize: 17,
//     },
//     buttonLogin: {
//         alignItems: 'center',
//         //backgroundColor: '#BA55D3',
//         padding: 10,
//         width: "80%",
//         height: 45,
//         backgroundColor: "rgba(360, 255, 0, 0.6)",
//         marginBottom: 10,
//     }
// });

export default RegistrationScreen;