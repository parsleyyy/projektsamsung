import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
  Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Card, CardItem, Text, Item, Input,
  Accordion, View, ActionSheet
} from 'native-base';
import { Image } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';

class TournamentDescriptionScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: '',
      message: '',
      started: false,
    };
    Navigation.events().bindComponent(this);

  };

  navigationButtonPressed({ buttonId }) {
    if (buttonId === "buttonOne") {
      Navigation.mergeOptions(this.props.componentId, {
        sideMenu: {
          right: {
            visible: true,
          }
        }
      });
    };
  };

  componentWillUnmount() {

  }


  componentDidMount() {

  };

  componentDidDisappear() {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: false,
        }
      }
    });
  }

  leaveTournament = () => {
    const { dispatch, user, tournamentDetails } = this.props;
    dispatch(reduxActions.leaveTournamentRequest(tournamentDetails._id, user.token));
  };

  _renderHeader(item, expanded) {
    return (
      <Grid style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', }}>
        <Col size={5}>
          <Text style={{ fontWeight: "bold", fontSize: 18, color: "black", padding: 15 }}>
            {item.title ? item.title : ""}
          </Text>
        </Col>
        <Col size={1}>
          {expanded
            ? <Icon style={{ fontSize: 30, textAlign: 'right', paddingRight: 10, paddingTop: 15 }} name="md-arrow-dropup" />
            : <Icon style={{ fontSize: 30, textAlign: 'right', paddingRight: 10, paddingTop: 15 }} name="md-arrow-dropdown" />}
        </Col>
      </Grid>
    );
  };

  _renderContent(item) {
    return (
      <Text style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', paddingBottom: 15, paddingLeft: 15, paddingTop: 15, paddingRight: 15 }}>{item.content}</Text>
    );
  };

  _renderButton(joined, started) {
    const JOIN_BUTTONS = [
      'Dołącz do turnieju',
      'Wróć',
    ];
    const LEAVE_BUTTONS = [
      'Opuść turniej',
      'Wróć',
    ];
    const CANCEL_INDEX = 1;

    if (joined === true) {
      if (started > 0) {
        return (
          <Button disabled>
            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>TURNIEJ ROZPOCZĘTY</Text>
          </Button>
        )
      } else {
        return (
          <>
            <Button
              full
              onPress={() =>
                ActionSheet.show(
                  {
                    options: LEAVE_BUTTONS,
                    cancelButtonIndex: CANCEL_INDEX,
                    title: "Czy na pewno?"
                  },
                  buttonIndex => {
                    this.setState({ clicked: LEAVE_BUTTONS[buttonIndex] });
                    if (buttonIndex == 0) {
                      this.leaveTournament();
                    };
                  }
                )
              }
            >
              <Text style={{ fontSize: 16, fontWeight: 'bold' }}>ZREZYGNUJ Z TURNIEJU</Text>
            </Button>
            <ActionSheet ref={(c) => { ActionSheet.actionsheetInstance = c; }} />
          </>
        )
      }
    } else {
      if (started > 0) {
        return (
          <Button disabled>
            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>TURNIEJ ROZPOCZĘTY</Text>
          </Button>
        )
      } else {
        if (this.props.tournamentDetails.players.length < this.props.tournamentDetails.maxPlayers) {
          return (
            <>
              <Button
                full
                onPress={() =>
                  Navigation.showModal({
                    stack: {
                      children: [{
                        component: {
                          name: 'games-app.JoinTournamentModal',
                          passProps: {
                            getTournamentId: this.props.tournamentDetails._id,
                          },
                          options: {
                            screenBackgroundColor: 'transparent',
                            modalPresentationStyle: 'overCurrentContext',
                          }
                        }
                      }]
                    }
                  })
                }
              >
                <Text style={{ fontSize: 16, fontWeight: 'bold' }}>DOŁĄCZ DO TURNIEJU</Text>
              </Button>
              <ActionSheet ref={(c) => { ActionSheet.actionsheetInstance = c; }} />
            </>
          )
        } else {
          return (
            <Button disabled>
              <Text style={{ fontSize: 16, fontWeight: 'bold' }}>OBECNIE KOMPLET GRACZY</Text>
            </Button>
          )
        }
      }
    }
  }

  render() {

    return (
      <Container>
        <Header style={{}}>
          <Text style={{ color: 'white', textTransform: 'uppercase', fontSize: 20, textAlign: 'center', alignSelf: 'center' }}>Szczegółowe informacje</Text>
        </Header>
        <Content padder>
          <Card>
            <Left>
              <Body>
                <Text style={{ textAlign: 'left', padding: 10, alignContent: 'flex-start', fontSize: 25, fontWeight: 'bold' }}>{this.props.tournamentDetails.name}</Text>
              </Body>
            </Left>
            <CardItem cardBody>
              <Image source={require('../../../images/chess6.jpg')} style={{ height: 200, width: null, flex: 1 }} />
            </CardItem>
            <CardItem style={{}}>
              <Grid style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', paddingBottom: 15 }}>
                <Row>
                  <Col size={2}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', paddingBottom: 10 }}>{`Gdzie? `}</Text>
                  </Col>
                  <Col size={1}>
                    <Row>
                      <Text>{`${this.props.tournamentDetails.city}`}</Text>
                    </Row>
                    <Row>
                      <Text>{`${this.props.tournamentDetails.zipCode}`}</Text>
                    </Row>
                    <Row>
                      <Text>{`${this.props.tournamentDetails.street}`}</Text>
                    </Row>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem >
              <Grid style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', paddingBottom: 15 }}>
                <Row>
                  <Col size={2}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', paddingBottom: 10 }}>{`Kiedy? `}</Text>
                  </Col>
                  <Col size={1}>
                    <Text>{`${this.props.tournamentDetails.date.substr(0, 10)} `}</Text>
                  </Col >
                </Row>
              </Grid>
            </CardItem>
            <CardItem>
              <Grid style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', paddingBottom: 15 }}>
                <Row>
                  <Col size={2}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', paddingBottom: 10 }}>{`Ranga `}</Text>
                  </Col>
                  <Col size={1}>
                    <Text>{`${this.props.tournamentDetails.rank} `}</Text>
                  </Col>
                </Row>
              </Grid>
            </CardItem>
            <CardItem >
              <Grid style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', paddingBottom: 15 }}>
                <Row>
                  <Col size={2}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', paddingBottom: 10 }}>{`Frekwencja `}</Text>
                  </Col>
                  <Col size={1}>
                    <Text>{`${this.props.tournamentDetails.players.length} / ${this.props.tournamentDetails.maxPlayers} `}</Text>
                  </Col >
                </Row>
              </Grid>
            </CardItem>
            <CardItem >
              <Grid >
                <Row>
                  <Col size={2}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', paddingBottom: 10 }}>{`Maksymalna ilość rund `}</Text>
                  </Col>
                  <Col size={1}>
                    <Text>{`${this.props.tournamentDetails.maxRounds}  `}</Text>
                  </Col >
                </Row>
              </Grid>
            </CardItem>
            <Accordion
              dataArray={[{ title: "Zasady", content: this.props.tournamentDetails.rules }, { title: "Opis", content: this.props.tournamentDetails.description }]}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              expanded={0}
              headerStyle={{ backgroundColor: "white" }}
            />
          </Card>
        </Content>
        <Footer>
          <FooterTab>
            {this.props.tournamentDetails ? this._renderButton(this.props.joined, this.props.tournamentDetails.rounds.length) : ''}
          </FooterTab>
        </Footer>
      </Container >

    )
  };
};

const mapStateToProps = state => {

  const tournamentDetails = state.tournaments.tournaments.filter(t => t._id === state.tournamentDetails.tournamentDetails)[0];
  const joined = ((tournamentDetails.players.filter(player => player.player._id == state.user.user.id)).length > 0);

  return ({
    user: state.user.user,
    tournaments: state.tournaments,
    tournamentDetails,
    response: state.tournaments.response,
    joined,
  })
};

const ConnectedTournamentDescriptionScreen = connect(mapStateToProps)(TournamentDescriptionScreen);

export default ConnectedTournamentDescriptionScreen;


