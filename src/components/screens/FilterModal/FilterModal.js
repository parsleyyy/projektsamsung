import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
  Container, Content, Footer, FooterTab, Button,
  Text, CheckBox, Picker, DatePicker
} from 'native-base';
import { TextInput, StyleSheet, ScrollView, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';
import DefaultHeader from '../UI/DefaultHeader';

const styles = StyleSheet.create({
  itemView: {
    flexDirection: 'row', 
    alignItems: 'center', 
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  border: {
    borderBottomColor: '#f0f0f5',
    borderBottomWidth: 1,
  },
  buttonClear: {
    width: '100%', 
    backgroundColor: "gray", 
    height: 53,
    position: 'absolute',
    top: 0,
  },
  input: {
    paddingLeft: 20,
  },
  icon: {
    fontSize: 30, 
    color: '#CF7322',
    flex: 0.1,
  },
  iconClear: {
    fontSize: 20, 
    color: 'white', 
    paddingLeft: 120,
  },
  textClear: {
    paddingTop: 10, 
    color: 'white', 
    paddingRight: 125, 
    paddingBottom: 10, 
    fontSize: 15,
  },
  mainText: {
    textTransform: 'uppercase',
  },
  bottomText: {
    color: 'white', 
    textTransform: 'uppercase', 
    fontSize: 15, 
    textAlign: 'center',  
    margin: 0,
    padding: 0,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: 55,
  },
});

class FilterModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
    Navigation.events().bindComponent(this);
    this.removeFilters = this.removeFilters.bind(this);
  };

  navigationButtonPressed({ buttonId }) {
    if (buttonId === "buttonOne") {
      Navigation.mergeOptions(this.props.componentId, {
        sideMenu: {
          right: {
            visible: true,
          },
        },
      });
    };
  };

  removeFilters() {
    const { dispatch } = this.props;
    dispatch(reduxActions.removeFilters());
  };

  render() {
    const ranks = [
      "Dowolna", "Challenger", "Master", "Lokalny", "Liga",
    ];

    return (
      <Container>
        <DefaultHeader name="Filtr" />
        <Content contentContainerStyle={styles.container}>
          <Button
            iconLeft
            style={styles.buttonClear}
            onPress={this.removeFilters}
          >
            <Icon name='md-close' style={styles.iconClear} />
            <Text style={styles.textClear}>Wyczyść filtry</Text>
          </Button>
          
          <ScrollView style={{ flex: 1 }}>
            <View>
              <View style={styles.border}>
                <View style={styles.itemView}>
                  <Icon style={styles.icon} name="md-checkmark-circle-outline" />
                  <Text style={[styles.mainText, {  flex: 0.8 }]}>Mogę przystąpić</Text>
                  <View style={{ flex: 0.1 }}>
                    <CheckBox
                      checked={this.props.filters.freePlaces}
                      onPress={() => {
                        const { dispatch } = this.props;
                        dispatch(reduxActions.setFilters('freePlaces', !this.props.filters.freePlaces));
                      }}
                    />
                  </View>
                </View>
              </View>

              <View style={styles.border}>
                <View style={styles.itemView}>
                  <Icon style={styles.icon} name="md-calendar" />
                  <Text style={[styles.mainText, { flex: 0.9,}]}>Szukaj w terminie:</Text>
                </View>
                <View style={[styles.itemView, {justifyContent: 'space-between'}]}>
                  <View>
                    <DatePicker
                      defaultDate={!this.props.filters.dateFrom ? Date.now() : this.props.filters.dateFrom}
                      minimumDate={new Date(2000, 12, 30)}
                      maximumDate={new Date(2020, 12, 30)}
                      locale={"en"}
                      timeZoneOffsetInMinutes={undefined}
                      modalTransparent={false}
                      animationType={"fade"}
                      androidMode={"default"}
                      placeHolderText="Szukaj od..."
                      textStyle={{ color: "green" }}
                      placeHolderTextStyle={{ color: "#d3d3d3" }}
                      onDateChange={(newDate) => {
                        const { dispatch } = this.props;
                        dispatch(reduxActions.setFilters('dateFrom', newDate));
                      }}
                      disabled={false}
                    />
                    <Text style={{ paddingLeft: 10,}}>
                      {`Od: ${this.props.filters.dateFrom ? this.props.filters.dateFrom.toString().substr(4, 12) : ''}`}
                    </Text>
                  </View>
                  <View>
                    <DatePicker
                      defaultDate={!this.props.filters.dateTo ? new Date(Date.now()) : this.props.filters.dateTo}
                      minimumDate={new Date(2000, 12, 30)}
                      maximumDate={new Date(2020, 12, 30)}
                      locale={"en"}
                      timeZoneOffsetInMinutes={undefined}
                      modalTransparent={false}
                      animationType={"fade"}
                      androidMode={"default"}
                      placeHolderText="Szukaj do..."
                      textStyle={{ color: "green" }}
                      placeHolderTextStyle={{ color: "#d3d3d3" }}
                      onDateChange={(newDate) => {
                        const { dispatch } = this.props;
                        dispatch(reduxActions.setFilters('dateTo', newDate));
                      }}
                      disabled={false}
                    />
                    <Text style={{ paddingLeft: 10,}}>
                      Do: {this.props.filters.dateTo ? this.props.filters.dateTo.toString().substr(4, 12) : ''}
                    </Text>
                  </View>
                </View>
              </View>
              
              <View style={styles.border}>
                <View style={styles.itemView}>
                  <Icon style={styles.icon} name="md-ribbon" />
                  <Text style={[styles.mainText, { flex: 0.5,}]}>Ranga:</Text>
                  <View style={{flex: 0.4}}>
                    <Picker
                      mode="dropdown"
                      headerStyle={{ backgroundColor: "#b95dd3" }}
                      headerBackButtonTextStyle={{ color: "#fff" }}
                      headerTitleStyle={{ color: "#fff" }}
                      selectedValue={this.props.filters.rank}
                      onValueChange={(value) => {
                        const { dispatch } = this.props;
                        dispatch(reduxActions.setFilters('rank', value));
                      }}
                    >
                      {ranks.map(rank => <Picker.Item label={rank} value={rank} />)}
                    </Picker>
                  </View>
                </View> 
              </View>

              <View style={styles.border}>
                <View style={styles.itemView}>
                  <Icon style={styles.icon} name="md-locate" />
                  <Text style={[styles.mainText, { flex: 0.9,}]}>Miasto:</Text>
                </View>
                <TextInput
                  style={styles.input}
                  placeholder='Wpisz nazwę miasta'
                  onChangeText={(text) => {
                    const { dispatch } = this.props;
                    dispatch(reduxActions.setFilters('city', text));
                  }}
                  value={this.props.filters.city}
                />
              </View>

              <View>
                <View style={styles.itemView}>
                  <Icon style={styles.icon} name="md-quote" />
                  <Text style={[styles.mainText, { flex: 0.9,}]}>Nazwa turnieju:</Text>
                </View>
                <TextInput
                  style={styles.input}
                  placeholder='Wpisz nazwę turnieju'
                  onChangeText={(text) => {
                    const { dispatch } = this.props;
                    dispatch(reduxActions.setFilters('name', text));
                  }}
                  value={this.props.filters.name}
                />
              </View>
            </View>
          </ScrollView>
        </Content >

        <Footer>
          <FooterTab>
            <Button
              vertical
              onPress={() => {
                Navigation.dismissModal(this.props.componentId);
              }}
            >
              <Text style={styles.bottomText}>ZAMKNIJ I SZUKAJ TURNIEJÓW</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container >
    );
  };

}

const mapStateToProps = state => ({
  user: state.user.user,
  tournaments: state.tournaments,
  tournamentDetails: state.tournamentDetails,
  getUserLoading: state.user.getUserLoading,
  filters: state.filters,
});


const ConnectedFilterModal = connect(mapStateToProps)(FilterModal);
export default ConnectedFilterModal;
