import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
    Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Accordion, Thumbnail,
    Text, Card, CardItem, Image, List, ListItem, View, Spinner
} from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';
import DefaultHeader from '../UI/DefaultHeader';

class GameHistoryScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        Navigation.events().bindComponent(this);
    };

    navigationButtonPressed({ buttonId }) {
        Navigation.mergeOptions(this.props.componentId, {
            sideMenu: {
                right: {
                    visible: true,
                },
            },
        });
    };

    componentDidMount() {
        const { dispatch, user } = this.props;
        dispatch(reduxActions.gameHistoryRequest(user.id, user.token));
    };

    _renderHeader(item, expanded) {
        const images = [
            require('../../../images/chess4.jpg'),
            require('../../../images/chess3.jpg'),
            require('../../../images/chess2.jpg'),
            require('../../../images/game7.jpg'),
            require('../../../images/game8.jpg'),
            require('../../../images/chess4.jpg'),
            require('../../../images/game5.jpg'),
            require('../../../images/game5.jpg'),
            require('../../../images/game4.jpg'),
            require('../../../images/game3.jpg'),
        ];

        return (
            <View style={{
                flexDirection: "row",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                backgroundColor: "white",
                borderColor: 'black',
                paddingBottom: 15,
            }}>

                <Grid style={{}}>
                    <Row style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', paddingBottom: 20 }}>
                        <Col size={1}>
                            <Thumbnail large style={{ borderColor: '#003399', borderWidth: 2, padding: 10 }} source={images[Math.floor(Math.random() * 10)]} />
                        </Col>
                        <Col size={4}>
                            <Row size={3}/>
                            <Row size={1}>
                                <Text style={{ fontSize: 22, color: "black", paddingLeft: 35, fontWeight: 'bold', }}>
                                    {item.tournament ? item.tournament.name : ""}
                                </Text>
                            </Row>
                            <Row size={1}>
                                <Text style={{ fontSize: 14, opacity: 0.5, paddingLeft: 35, paddingBottom: 30, }}>
                                    {`${item.playerA.player ? item.playerA.player.firstName : ""} ${item.playerA.player ? item.playerA.player.lastName : ""} vs ${item.playerB.player ? item.playerB.player.firstName : ""} ${item.playerB.player ? item.playerB.player.lastName : ""}`}
                                </Text>
                            </Row>
                            <Row size={3}></Row>
                        </Col>
                        <Col size={1}>
                            {expanded
                                ? <View style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 25 }}><Icon style={{ fontSize: 30, textAlign: 'right', paddingRight: 10 }} name="md-arrow-dropup" /></View>
                                : <View style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 25 }}><Icon style={{ fontSize: 30, textAlign: 'right', paddingRight: 10 }} name="md-arrow-dropdown" /></View>}
                        </Col>
                    </Row>
                </Grid>
            </View>
        );
    };

    _renderContent(item) {
        return (
            <View bordered>
                <Grid >
                    <Row style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', paddingBottom: 20 }}>
                        <Col size={4}>
                            <Row>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 25, paddingRight: 0, textAlign: 'right', textTransform: 'uppercase', color: '#0D347D' }}>
                                        {`${item.playerA.player ? item.playerA.player.firstName : ""} `}
                                    </Text>
                                </View>
                            </Row>
                            <Row>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontWeight: 'bold', paddingRight: 0, fontSize: 25, color: '#0D347D', textAlign: 'right' }}>
                                        {`${item.playerA.player ? item.playerA.score : ""} `}
                                    </Text>
                                </View>
                            </Row>
                        </Col>
                        <Col size={1}>
                            <View style={{ flex: 1 }}>
                                <Text note style={{ textAlign: 'center', fontSize: 14, paddingTop: 7 }}>VS</Text>
                            </View>
                            <View style={{ flex: 1 }}>
                                <Text note style={{ textAlign: 'center', fontSize: 14, paddingTop: 7 }}>/</Text>
                            </View>
                        </Col>
                        <Col size={4}>
                            <Row>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 25, paddingLeft: 7, textAlign: 'left', textTransform: 'uppercase', color: '#604108' }}>
                                        {`${item.playerB.player ? item.playerB.player.firstName : ""} `}
                                    </Text>
                                </View>
                            </Row>
                            <Row>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 25, paddingLeft: 5, color: '#604108', textAlign: 'left' }}>
                                        {`${item.playerB.player ? item.playerB.score : ""} `}
                                    </Text>
                                </View>
                            </Row>
                        </Col>
                    </Row>

                    <Row style={{ borderBottomWidth: 0.5, borderBottomColor: '#d6d7da', paddingBottom: 20, paddingTop: 20 }}>
                        <Col>
                            <Row>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontWeight: 'bold', textAlign: 'right', color: '#1A58B7', paddingRight: 8, }}>{`DETACHMENTS: `}</Text>
                                </View>
                            </Row>
                            <Row style={{ paddingBottom: 10 }}>
                                {item.playerA ?
                                    <List
                                        dataArray={item.playerA.detachments}
                                        renderRow={detachment => (
                                            <>
                                                {detachment.keyword ? <View style={{ flex: 1 }}><Text style={{ textAlign: 'right', color: '#1A58B7', paddingRight: 8, }}>{detachment.keyword}</Text></View> : ""}
                                                {/* {detachment.withWarlord === true ? <Text>With Warlord</Text> : ""}  */}
                                            </>
                                        )}
                                    >
                                    </List>
                                    :
                                    <>
                                    </>
                                }
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ textAlign: 'left', fontWeight: 'bold', color: '#B7851A', paddingLeft: 8, paddingBottom: 10 }}>{`DETACHMENTS: `}</Text>
                                </View>
                            </Row>
                            <Row style={{ paddingBottom: 20 }}>

                                {item.playerB ?
                                    <List
                                        dataArray={item.playerB.detachments}
                                        renderRow={detachment => (
                                            <>
                                                {detachment.keyword ? <View style={{ flex: 1 }}><Text style={{ textAlign: 'left', paddingLeft: 8, color: '#B7851A' }}>{detachment.keyword}</Text></View> : ""}
                                                {/* {detachment.withWarlord === true ? <Text>With Warlord</Text> : ""}  */}
                                            </>
                                        )}
                                    >
                                    </List>
                                    :
                                    <>
                                    </>
                                }
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </View >
        );
    }

    render() {
        const { gameHistory } = this.props;

        return (
            <Container>
                <DefaultHeader name='Historia Twoich meczy' />
                <Content padder>
                    {!this.props.gameHistoryLoading ?
                        <>
                            {gameHistory.match[0] && Object.keys(gameHistory.match[0]).length > 0 ?
                                <Accordion
                                    dataArray={gameHistory.match}
                                    renderHeader={this._renderHeader}
                                    renderContent={this._renderContent}
                                    expanded={0}
                                    headerStyle={{ backgroundColor: "white", }}
                                />
                                :
                                <Card>
                                    <CardItem>
                                        <Text>Brak zarchiwizowych turniejów</Text>
                                    </CardItem>
                                </Card>
                            }
                        </>
                        :
                        <Spinner color='blue' />
                    }
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    gameHistory: state.gameHistory.gameHistory,
    gameHistoryLoading: state.gameHistory.gameHistoryLoading,
});

const ConnectedGameHistoryScreen = connect(mapStateToProps)(GameHistoryScreen);

export default ConnectedGameHistoryScreen;