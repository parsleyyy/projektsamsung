import React from 'react';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

const SetMain = () => {
  Promise.all([
    Icon.getImageSource('md-menu', 20),
  ]).then((sources) => {
    Navigation.setRoot({
      root: {
        sideMenu: {
          right: {
            component: {
              id: "rightDrawer",
              name: "games-app.SideMenu",
            }
          },
          center: {
            stack: {
              id: "AppRoot",
              title: "Main Screen",
              children: [{
                component: {
                  id: "App",
                  name: "games-app.MainScreen",
                  options: {
                    topBar: {
                      rightButtons: [
                        {
                          id: 'buttonOne',
                          icon: sources[0],
                        }
                      ],

                    }
                  }
                }
              }]
            }
          }
        }
      }
    })
  });
};

export default SetMain;