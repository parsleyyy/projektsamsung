import { Navigation } from 'react-native-navigation';

const SetAuth = () => {
    Navigation.setRoot({
        root: {
            stack: {
                id: "mainStack",
                children: [{
                    component: {
                        name: "games-app.AuthenticationScreen"
                    }
                }]
            }
        }
    });
};

export default SetAuth;