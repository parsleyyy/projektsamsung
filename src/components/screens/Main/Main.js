import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
  Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Accordion, Thumbnail,
  Text, Card, CardItem, Image, List, ListItem, Spinner,
  Tab, Tabs, TabHeading, View
} from 'native-base';
import { FlatList } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';
import MainService from '../../../services/mainService';
import DefaultHeader from '../UI/DefaultHeader';
import TournamentItem from '../UI/TournamentItem';

class MainScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filtredData: '',
      loaded: false,
    };

    MainService.load(() => this.setState({ loaded: true }));
    Navigation.events().bindComponent(this);
    this._renderItem = this._renderItem.bind(this);
    this._onPressItem = this._onPressItem.bind(this);
    this._keyExtractor = this._keyExtractor.bind(this);
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === "buttonOne") {
      Navigation.mergeOptions(this.props.componentId, {
        sideMenu: {
          right: {
            visible: true,
          },
        },
      });
    };
  };

  componentDidMount() {
    const { dispatch, user } = this.props;
    dispatch(reduxActions.getTournamentRequest(user.token));
  };

  componentDidDisappear() {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: false,
        },
      },
    });
  };

  _keyExtractor(item, index) {
    return item._id;
  };

  _onPressItem(_id) {
    const { dispatch } = this.props;
    dispatch(reduxActions.getTournamentDetails(_id));

    Navigation.mergeOptions('rightDrawer', {
      sideMenu: {
        right: {
          visible: false,
        },
      },
    });

    Promise.all([
      Icon.getImageSource('md-menu', 20),
    ]).then((sources) => {
      Navigation.push(this.props.componentId, {
        component: {
          name: 'games-app.TournamentDescriptionScreen',
          options: {
            topBar: {
              rightButtons: [
                {
                  id: 'buttonOne',
                  icon: sources[0]
                }
              ]
            }
          },
          passProps: {
            token: this.props.user.token,
          },
        },
      });
    });
  };

  _renderItem(item) {
    return (
      <TournamentItem
        onPressItem={this._onPressItem}
        tournament={item}
        user={this.props.user}
        id={item._id}
      />
    );
  };


  render() {
    return (
      <Container>
        <DefaultHeader name="Aktualna lista turniejów" />

        {this.props.getTournamentLoading ?
          <Spinner color='blue' />
          :
          <>
            <Content padder>
              <View style={{ paddingBottom: 15 }} >
                <Button
                  iconLeft
                  full
                  big
                  style={{ width: '100%', backgroundColor: '#f2f2f2', }}
                  onPress={() => {
                    Navigation.mergeOptions('rightDrawer', {
                      sideMenu: {
                        right: {
                          visible: false,
                        }
                      }
                    });

                    Navigation.showModal({
                      stack: {
                        children: [{
                          component: {
                            name: 'games-app.FilterModal',
                            passProps: {
                              getTournamentId: this.props.tournamentDetails._id,
                            },
                            options: {
                              screenBackgroundColor: 'transparent',
                              modalPresentationStyle: 'overCurrentContext',
                            }
                          }
                        }]
                      }
                    });
                  }}
                >
                  <Icon name='md-search' style={{ fontSize: 30, }} />
                  <Text style={{ color: 'black', }}>FILTRUJ TURNIEJE</Text>
                </Button>
              </View>
              {this.props.filteredTournaments.length ?
                <FlatList
                  data={this.props.filteredTournaments}
                  renderItem={this._renderItem}
                  keyExtractor={this._keyExtractor}              
                />
                :
                    <Card>
                      <CardItem>
                        <Text>Obecnie nie znaleziono żadnego turnieju</Text>
                      </CardItem>
                    </Card>
              }
            </Content>
          </>
        }
      </Container>
    );
  };

}

const mapStateToProps = state => {
    const filteredTournaments = state.tournaments.tournaments.filter(t =>
      t.name.toLowerCase().includes(state.filters.name.toLowerCase())
      && t.city.toLowerCase().includes(state.filters.city.toLowerCase())
      && (state.filters.freePlaces ? (t.rounds.length === 0 && t.players.length < t.maxPlayers) : true)
      && (state.filters.rank === "Dowolna" ? true : (t.rank == state.filters.rank))
      && (state.filters.dateFrom === undefined ? true : state.filters.dateFrom < new Date(t.date))
      && (state.filters.dateTo === undefined ? true : state.filters.dateTo > new Date(t.date)))

  return ({
    user: state.user.user,
    tournaments: state.tournaments,
    tournamentDetails: state.tournamentDetails,
    getUserLoading: state.user.getUserLoading,
    getTournamenLoading: state.tournaments.getTournamenLoading,
    filters: state.filters,
    filteredTournaments,
  })
};


const ConnectedMain = connect(mapStateToProps)(MainScreen);
export default ConnectedMain;
