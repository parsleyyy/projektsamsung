import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
  Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Accordion, Thumbnail,
  Text, Card, CardItem, Image, List, ListItem, Spinner,
  Tab, Tabs, TabHeading, View
} from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import { TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';
import MainService from '../../../services/mainService';

const styles = StyleSheet.create({
  blueText: {
    color: 'blue',
  },
  redText: {
    color: 'red',
  },
  greenText: {
    color: '#003399', 
  },
  item: {
    marginBottom: 20,
  },
});

const _renderStatusText = (rounds, players, maxPlayers) => {
  return (
    <>
      {rounds.length ?
        <Text style={styles.blueText}>Turniej rozpoczęty</Text>
        :
        <>
          {players.length >= 0 ?
            <>
              {players.filter(player => player.player._id == props.user.id).length > 0 ?
                <Text style={[styles.blueText, { fontWeight: 'bold' }]}>CZEKAMY NA CIEBIE :)</Text>
                :
                <>
                  {players.length < maxPlayers ?
                    <Text style={styles.greenText}>Dołącz do turnieju!</Text>
                    :
                    <Text style={styles.redText}>Obecnie komplet graczy</Text>
                  }
                </>
              }
            </>
            :
            <>
            </>
          }
        </>
      }
    </>
  )
};

const images = [
  require('../../../images/chess4.jpg'),
  require('../../../images/chess3.jpg'),
  require('../../../images/chess2.jpg'),
  require('../../../images/game7.jpg'),
  require('../../../images/game8.jpg'),
  require('../../../images/chess4.jpg'),
  require('../../../images/game5.jpg'),
  require('../../../images/game5.jpg'),
  require('../../../images/game4.jpg'),
  require('../../../images/game3.jpg'),
];

const TournamentItem = props => (
  <TouchableOpacity style={styles.item}>
    <Grid>
      <Col size={2}>
        <Thumbnail large style={{ borderColor: '#003399', borderWidth: 2 }} source={images[Math.floor(Math.random() * 10)]} />
      </Col>
      <Col size={4}>
        <Row><Text style={{ color: '#B7851A', fontWeight: 'bold' }}>{`${props.tournament.item.name}`}</Text></Row>
        <Row><Text style={{ opacity: 0.5 }}>{props.tournament.item.city}</Text></Row>
        <Row><Text style={{ opacity: 0.5 }}>{props.tournament.item.date.substr(0, 10)}</Text></Row>
        {/* <Row>
          {() => _renderStatusText(props.tournament.item.rounds, props.tournament.item.players, props.tournament.item.maxPlayers)}
        </Row> */}
      </Col>
      <Col size={1} style={{}}>
        <Right>
          <View style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 30 }}><Icon style={{ fontSize: 18, }} name="md-arrow-forward" /></View>
        </Right>
      </Col>
    </Grid>
  </TouchableOpacity>
);

export default TournamentItem;
