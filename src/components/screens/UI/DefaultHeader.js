import React from 'react';
import { StyleSheet } from 'react-native';
import {
    Header, Text
} from 'native-base';
import { View } from 'react-native';

const styles = StyleSheet.create({
    header: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color: 'white', 
        textTransform: 'uppercase',
        fontSize: 20,
        paddingTop: 15,
    },
});

const DefaultHeader = props => (
    <Header>
        <View contentContainerStyle={styles.header}>
            <Text style={styles.title}>{props.name}</Text>
        </View>
    </Header>
);
export default DefaultHeader;