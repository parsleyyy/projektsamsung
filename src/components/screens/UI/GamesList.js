import React from 'react';
import { Navigation } from 'react-native-navigation';
import {
  Button, Thumbnail, Text, List, Spinner, View
} from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
  maintext: {
    fontWeight: "500",
    fontSize: 30,
    color: 'black',
    textAlign: 'left',
    paddingLeft: 10,
  },
  mainRow: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#d6d7da',
    paddingBottom: 15,
  },
  thumbnail: {
    padding: 10,
  },
  textGamers: {
    fontSize: 15,
    opacity: 0.5,
    paddingLeft: 10,
  },
  mainView: {
    flex: 1,
  },
  textLabelRight: {
    paddingTop: 15,
    paddingRight: 8,
    textTransform: 'uppercase',
    textAlign: 'right',
    fontWeight: 'bold',
  },
  textLabelLeft: {
    paddingTop: 15,
    textAlign: 'left',
    paddingLeft: 8,
  },
  textName: {
    fontWeight: 'bold',
    fontSize: 22,
    paddingRight: 8,
    paddingBottom: 2,
    color: '#0D347D',
    textTransform: 'uppercase',
    textAlign: 'right',
  },
  textDetachments: {
    fontWeight: 'bold',
    paddingRight: 8,
    textAlign: 'right',
    color: '#1A58B7',
  },
  detachmentsLeft: {
    textAlign: 'right',
    color: '#1A58B7',
    paddingRight: 10,
  },
  detachmentsRight: {
    paddingLeft: 8,
    textAlign: 'left',
    color: '#B7851A',
  },
  icon: {
    textAlign: 'right',
    fontSize: 25,
    color: 'white',
  },
});

const Label = (props) => {
  const { name, value } = props;

  return (
    <>
      <Col>
        <View style={styles.mainView}>
          <Text style={styles.textLabelRight}>{`${name}: `}</Text>
        </View>
      </Col>
      <Col>
        <View style={styles.mainView}>
          <Text note style={styles.textLabelLeft}>{value}</Text>
        </View>
      </Col>
    </>
  );
};

const Gamer = (props) => {
  const { name, detachments, side } = props;

  return (
    <Col>
      <Row>
        <View style={{ flex: 1 }}>
          <Text style={styles.textName}>
            {`${name || ''} `}
          </Text>
        </View>
      </Row>
      <Row>
        <View style={{ flex: 1 }}>
          <Text style={styles.textDetachments}>DETACHMENTS: </Text>
        </View>
      </Row>
      <Row>
        <FlatList
          data={detachments}
          renderItem={detachment => (
            <View style={styles.mainView}>
              <Text style={styles[side]}>
                {detachment.keyword || ''}
              </Text>
            </View>
          )}
        />
      </Row>
    </Col>
  );
};

const GamesList = (props) => {
  const { name, playerA, playerB, table } = props.tournament.item;

  return (
    <View>
      <Grid>
        <Row style={styles.mainRow}>
          <Col size={1}>
            <Thumbnail large style={styles.thumbnail} source={require('../../../images/chess7.jpg')} />
          </Col>
          <Col size={3}>
            <Row size={2} />

            <Row size={2}>
              <Text style={styles.maintext}>
                {name || ''}
              </Text>
            </Row>
            <Row size={2}>
              <Text note style={styles.textGamers}>
                {`${playerA.player || ''} ${playerA.player.lastName || ''} vs ${playerB.player.firstName || ''} ${playerB.player.lastName || ''}`}
              </Text>
            </Row>
            <Row size={2} />
          </Col>
        </Row>

        <Row>
          <Label name="NUMER STOŁU" value={table} />
        </Row>
        <Row>
          <Label name="RUNDA" value="numer rundy" />
        </Row>

        <Row>
          <Gamer name={playerA.player.firstName} detachments={playerA.detachments} side="detachmentsLeft" />
          <Gamer name={playerB.player.firstName} detachments={playerB.detachments} side="detachmentsRight" />
        </Row>

        <Row style={styles.mainRow}>
          <Button rounded
            iconRight
            style={{ width: '95%', flex: 1 }}
            onPress={() => {
              Navigation.showModal({
                stack: {
                  children: [{
                    component: {
                      name: 'games-app.AddResultModal',
                      passProps: {
                        getTournamentId: item._id,
                      },
                      options: {
                        screenBackgroundColor: 'transparent',
                        modalPresentationStyle: 'overCurrentContext',
                      },
                    },
                  }],
                },
              });
            }}
          >
            <Text style={{ textAlign: 'center' }}>ZGŁOŚ WYNIK</Text>
            <Icon style={styles.icon} name="md-done-all" />
          </Button>
        </Row>
      </Grid>
    </View>
  );
};

export default GamesList;
