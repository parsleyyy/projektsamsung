import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
  Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Card, CardItem, Text, Item, Input,
  Accordion, ActionSheet, List, View, ListItem, Thumbnail,
  Spinner,
} from 'native-base';
import { FlatList } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';
import DefaultHeader from '../UI/DefaultHeader';
import GamesList from '../UI/GamesList';

class ActiveGameScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    Navigation.events().bindComponent(this);
    this.setModalVisible = this.setModalVisible.bind(this);
  };

  componentDidMount() {
    const { dispatch, user } = this.props;
    dispatch(reduxActions.activeGameRequest(user.id, user.token));
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  };

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'buttonOne') {
      Navigation.mergeOptions(this.props.componentId, {
        sideMenu: {
          right: {
            visible: true,
          },
        },
      });
    };
  };

  componentDidDisappear() {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        right: {
          visible: false,
        },
      },
    });
  };

  _renderItem(item) {
    <GamesList
      tournament={item}
      user={this.props.user}
    />
  };

  render() {
    const { activeGame } = this.props;

    return (
      <Container>
        <DefaultHeader name="Aktywne gry" />
        <Content padder>
          {!this.props.activeGameLoading ?
            <>
              {activeGame.match[0] && Object.keys(activeGame.match[0]).length > 0 ?
                <FlatList
                  data={activeGame.match}
                  keyExtractor={this._keyExtractor}
                  renderItem={this._renderItem}
                />
                :
                <Card>
                  <CardItem>
                    <Text>Brak aktywnych gier</Text>
                  </CardItem>
                </Card>
              }
            </> :
            <Spinner color="blue" />
          }
        </Content>
      </Container>
    );
  };
};

const mapStateToProps = state => ({
  user: state.user.user,
  activeGame: state.activeGame.activeGame,
  activeGameLoading: state.activeGame.activeGameLoading,
});

const ConnectedActiveGameScreen = connect(mapStateToProps)(ActiveGameScreen);

export default ConnectedActiveGameScreen;
