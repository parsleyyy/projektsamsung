import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
    Container, Body, Content, Button,
    Card, CardItem, Text, Item, Input,
    Spinner, View,
} from 'native-base';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import storage from '../../../store/asyncStorage/asyncStorage';
import { SetAuth, SetMain } from '../SetRoot';
import reduxActions from '../../../store/actions/login';

const styles = StyleSheet.create({
    main: {
        flex: 1, 
        justifyContent: 'center',
        alignItems: 'center',
    },

});

class AuthenticationScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            loaded: false,
        };
        Navigation.events().bindComponent(this);
        console.disableYellowBox = true;
        this._retrieveData = this._retrieveData.bind(this);
        this.updateValue = this.updateValue.bind(this);
        this.loginHandler = this.loginHandler.bind(this);
    };

    componentDidMount() {
        this._retrieveData();
    };

    _retrieveData() {
        const { dispatch } = this.props;
        storage.getUserData()
            .then((user) => {
                if (user.token !== null) {
                    dispatch(reduxActions.setUser(user));
                    SetMain();
                }
            })
            .catch(error => {
                this.setState({ error });
            });
    };

    updateValue(text, field) {
        this.setState({
            [field]: text,
        });
    };

    loginHandler() {
        const { email, password } = this.state;
        const authData = {
            email: email,
            password: password,
        };

        const { dispatch } = this.props;
        dispatch(reduxActions.getUserRequest(authData));
    };

    render() {
        return (
            <Container >
                <Content contentContainerStyle={styles.main}>
                    <Card >
                        <CardItem style={{ marginBottom: 30, }}>
                            <Body style={{ justifyContent: 'center', }}>
                                <Text style={{ textAlign: 'center', marginBottom: 10, justifyContent: 'center', alignSelf: 'center', fontSize: 35, }}>Witamy!</Text>
                                <Text note style={{ color: '#6666ff', textAlign: 'center', justifyContent: 'center', alignSelf: 'center', fontSize: 15, }}>Zostań częścią naszej społeczności!</Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                                <Item rounded>
                                    <Input
                                        style={{ marginLeft: 15, }}
                                        placeholder='E-Mail Address'
                                        onChangeText={(text) => this.updateValue(text, 'email')}
                                        value={this.state.email}
                                    />
                                </Item>
                        </CardItem>
                        <CardItem>
                            <Item rounded>
                                <Input
                                    style={{ marginLeft: 15, }}
                                    placeholder="Password"
                                    onChangeText={(text) => this.updateValue(text, 'password')}
                                    value={this.state.password}
                                    secureTextEntry={true}
                                />
                            </Item>
                        </CardItem>
                        <CardItem style={{ marginBottom: 30, }}>
                            <Button
                                rounded
                                style={{ width: "100%", justifyContent: 'center' }}
                                onPress={() => this.loginHandler("games-app.MainScreen")}
                            >
                                <Text style={{ textAlign: "center", justifyContent: 'center', fontSize: 15, fontWeight: 'bold' }} > Sign In</Text>

                            </Button>
                        </CardItem>
                        <CardItem>
                            {this.props.user.getUserError ? <Text style={{ color: 'red' }}>Niepoprawny login lub hasło</Text> : <></>}
                        </CardItem>
                        <CardItem footer style={{ marginLeft: 10, }}>
                            <Body>
                                <Text >Don't have an account?</Text>
                                <Text
                                    style={{ fontWeight: 'bold' }}
                                    onPress={() => {
                                        Navigation.push(this.props.componentId, {
                                            component: {
                                                name: 'games-app.RegistrationScreen',
                                            }
                                        })
                                    }}
                                >
                                    Sign up here
                        </Text>
                            </Body>
                        </CardItem>
                    </Card>
                </Content>
            </Container >
        )
    };
};

const mapStateToProps = state => ({
    user: state.user,
    getUserLoading: state.user.getUserLoading,
});

const connectedAuthScreen = connect(mapStateToProps)(AuthenticationScreen)
export default connectedAuthScreen;