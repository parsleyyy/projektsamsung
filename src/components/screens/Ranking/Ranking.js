import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
    Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Accordion, Thumbnail,
    Text, Card, CardItem, Image, List, ListItem, Spinner,
} from 'native-base';
import { Flatlist, ListView, StyleSheet, View } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
//import GET_TOURNAMENT_DETAILS  from '../../../store/reducers/login';
import reduxActions from '../../../store/actions/login';

class RankingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        Navigation.events().bindComponent(this);
    };

    renderSectionHeader(sectionData, sectionID) {
        return (
            <ListItem >
                <Text>rankinggggg</Text>
            </ListItem>
        );
    }

    selectIcon(index) {
        if (index === 0) return <Icon rounded style={{ fontSize: 50, marginLeft: 25, color: '#cc9900' }} name="md-trophy" />
        else if (index === 1) return <Icon style={{ fontSize: 35, marginLeft: 29, color: '#a3a3c2' }} name="md-trophy" />
        else if (index === 2) return <Icon style={{ fontSize: 23, marginLeft: 34, color: '#663300' }} name="md-trophy" />
        else return <Text>{index + 1}</Text>
    }

    renderSectionHeader(sectionData, sectionID) {
        return (
            <ListItem >
                <Grid>
                    <Col size={30}><Text style={{ fontWeight: 'bold' }}>MIEJSCE</Text></Col>
                    <Col size={20}><Text style={{ fontWeight: 'bold' }}>WYNIK</Text></Col>
                    <Col size={60}><Text style={{ fontWeight: 'bold' }}>NAZWA GRACZA</Text></Col>
                </Grid>
            </ListItem>
        );
    }



    render() {
        console.log(this.props.ranking);
        const colors = ['#e6f0ff', 'white'];

        return (
            <Container>
                <Header style={{}}>
                    <Text style={{ color: 'white', textTransform: 'uppercase', fontSize: 20, textAlign: 'center', alignSelf: 'center' }}>Ranking graczy</Text>
                </Header>

                {this.props.ranking ?
                    <Content >
                        <List
                            dataArray={this.props.ranking}
                            renderSectionHeader={this.renderSectionHeader}
                            renderRow={(item) => {
                                return (
                                    <ListItem style={{ backgroundColor: colors[this.props.ranking.indexOf(item) % 2] }}>
                                        <Grid>
                                            <Col size={30} style={{ alignSelf: 'flex-end', paddingTop: 0, paddingBottom: 0, height: 35, width: 35, justifyContent: "center", }}>
                                                {this.selectIcon(this.props.ranking.indexOf(item))}
                                            </Col>
                                            <Col size={20} style={styles.rankingleft}>
                                                <Text>{item.mmr}</Text>
                                            </Col>
                                            <Col size={60}>
                                                <Text style={{ paddingTop: 5, fontWeight: 'bold' }}>{`${item.firstName} ${item.lastName}`}</Text>

                                            </Col>
                                        </Grid>
                                    </ListItem>
                                )
                            }}
                        >
                        </List>
                    </Content>
                    :
                    <Spinner color='blue' />
                }

            </Container>
        );
    };
};

const styles = StyleSheet.create({
    rankingleft: {
        paddingTop: 5,
    },

});

const mapStateToProps = state => ({
    user: state.user.user,
    ranking: state.ranking.ranking,
});

const ConnectedRanking = connect(mapStateToProps)(RankingScreen);
export default ConnectedRanking;

