import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
    Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Card, CardItem, Text,
    Accordion, View, ActionSheet, List, ListItem, Item, Input, Icon, CheckBox,
    Form, Picker
} from 'native-base';
import { TextInput } from 'react-native';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';
import { Col, Row, Grid } from "react-native-easy-grid";

class JoinTournamentModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clicked: "",
            withWarlord: false,
            roster: '',
        };
        Navigation.events().bindComponent(this);
    };

    componentDidMount() {
        if (this.props.tournamentDetails.detachmentLimit) {
            for (let j = 0; j < this.props.tournamentDetails.detachmentLimit; ++j) {
                this.setState({
                    ['checkbox' + j]: false,
                    ['selected' + j]: 'Nie wybrano',
                });
            };
        };
    };

    componentDidDisappear() {
        if (this.props.tournamentDetails.detachmentLimit) {
            for (let j = 0; j < this.props.tournamentDetails.detachmentLimit; ++j) {
                this.setState({
                    ['checkbox' + j]: false,
                    ['selected' + j]: 'Nie wybrano',
                });
            };
        };
    }

    updateValue(text, field) {
        this.setState({
            [field]: text,
        });
    };

    joinTournament = () => {
        const { dispatch, user, tournamentDetails } = this.props;
        dispatch(reduxActions.joinTournamentRequest(tournamentDetails._id, user.token));
        this.setState({
            joined: !this.state.joined,
        });
    };

    onValueChange(value, key) {
        const selected = 'selected' + key;
        this.setState({
            [selected]: value,
        });
    };

    onCheckChange(i) {
        for (let j = 0; j < this.props.tournamentDetails.detachmentLimit; ++j) {
            this.setState({
                ['checkbox' + j]: false,
            });
        };

        this.setState({
            ['checkbox' + i]: true,
        });
    };

    render() {
        const ADD_RESULT_BUTTONS = [
            'Dołącz',
            'Wróć',
        ];
        const CANCEL_INDEX = 1;
        console.log(this.state);

        return (
            <Container>
                <Header style={{}}>
                    <Text style={{ color: 'white', textTransform: 'uppercase', fontSize: 20, textAlign: 'center', alignSelf: 'center' }}>Uzupełnij szczegóły i dołącz</Text>
                </Header>
                <Content padder>
                    <Card>
                        {[...Array(this.props.tournamentDetails.detachmentLimit).keys()].map(i => (
                            <Form key={i}>
                                <ListItem>
                                    <Grid>
                                        <Col size={2}>
                                            <Text style={{ color: '#cc6600', fontWeight: 'bold', paddingTop: 15 }}>{`DETACHMENT ${i + 1}`}</Text>
                                        </Col>
                                        <Col size={1}>
                                        </Col>
                                        <Col size={2}>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="arrow-down" />}
                                                headerStyle={{ backgroundColor: "#b95dd3" }}
                                                headerBackButtonTextStyle={{ color: "#fff" }}
                                                headerTitleStyle={{ color: "#fff" }}
                                                selectedValue={this.state['selected' + i]}
                                                onValueChange={(value) => this.onValueChange(value, i)}
                                            >
                                                <Picker.Item label="Nie wybrano" value="Nie wybrano" />
                                                <Picker.Item label="Adeptus Mechanicus" value="Adeptus Mechanicus" />
                                                <Picker.Item label="Adeptus Ministorum" value="Adeptus Ministorum" />
                                                <Picker.Item label="Astra Militarum" value="Astra Militarum" />
                                                <Picker.Item label="Astra Telepathica" value="Astra Telepathica" />
                                                <Picker.Item label="Asuryani" value="Asuryani" />
                                                <Picker.Item label="Blood Angels" value="Blood Angels" />
                                                <Picker.Item label="Chaos Daemons" value="Chaos Daemons" />
                                                <Picker.Item label="Dark Angels" value="Dark Angels" />
                                                <Picker.Item label="Death Guard" value="Death Guard" />
                                                <Picker.Item label="Deathwatch" value="Deathwatch" />
                                                <Picker.Item label="Drukhari" value="Drukhari" />
                                                <Picker.Item label="Genestealer Cult" value="Genestealer Cult" />
                                                <Picker.Item label="Grey Knights" value="Grey Knights" />
                                                <Picker.Item label="Harlequins" value="Harlequins" />
                                                <Picker.Item label="Heretic Astartes" value="Heretic Astartes" />
                                                <Picker.Item label="Inquisition" value="Inquisition" />
                                                <Picker.Item label="Legion of the Damned" value="Legion of the Damned" />
                                                <Picker.Item label="Necron" value="Necron" />
                                                <Picker.Item label="Officio Assassinorum" value="Officio Assassinorum" />
                                                <Picker.Item label="Orks" value="Orks" />
                                                <Picker.Item label="Questor Imperialis" value="Questor Imperialis" />
                                                <Picker.Item label="Questor Traitoris" value="Questor Traitoris" />
                                                <Picker.Item label="Sisters of Silence" value="Sisters of Silence" />
                                                <Picker.Item label="Space Marines" value="Space Marines" />
                                                <Picker.Item label="Space Wolves" value="Space Wolves" />
                                                <Picker.Item label="T'au Empire" value="T'au Empire" />
                                                <Picker.Item label="Thousand Sons" value="Thousand Sons" />
                                                <Picker.Item label="Tyranids" value="Tyranids" />
                                                <Picker.Item label="Ynnari" value="Ynnari" />
                                            </Picker>
                                        </Col>
                                    </Grid>
                                </ListItem>
                                <ListItem >
                                    <CheckBox
                                        onPress={() => this.onCheckChange(i)}
                                        checked={this.state['checkbox' + i]}
                                    />
                                    <Body>
                                        <Text>Warlord</Text>
                                    </Body>
                                </ListItem>
                            </Form>
                        ))}
                        <ListItem>
                            <Grid>
                                <Row>
                                    <Text style={{ color: '#cc6600', fontWeight: 'bold', paddingTop: 10, paddingLeft: 10, paddingBottom: 5 }}>{`ROSTER `}</Text>
                                </Row>
                                <Row>
                                    <TextInput
                                        multiline={true}
                                        style={{ marginLeft: 20, paddingBottom: 5 }}
                                        placeholder='Miejsce na rozpiskę'
                                        onChangeText={(text) => this.setState({ roster: text })}
                                        value={this.state.email}
                                    />
                                </Row>
                            </Grid>
                        </ListItem>
                    </Card>
                </Content>
                <Footer>
                    <FooterTab>
                        <ActionSheet ref={(c) => { ActionSheet.actionsheetInstance = c; }} />
                        <Button vertical
                            onPress={() => {
                                ActionSheet.show(
                                    {
                                        options: ADD_RESULT_BUTTONS,
                                        cancelButtonIndex: CANCEL_INDEX,
                                        title: "Czy na pewno?"
                                    },
                                    buttonIndex => {
                                        this.setState({ clicked: ADD_RESULT_BUTTONS[buttonIndex] });
                                        if (buttonIndex == 0) {
                                            const { tournamentDetails, user } = this.props;
                                            const bodyContent = {
                                                roster: this.state.roster,
                                                detachments: [],
                                            };

                                            for (let j = 0; j < tournamentDetails.detachmentLimit; ++j) {
                                                bodyContent.detachments[j] = {
                                                    keyword: this.state['selected' + j],
                                                    withWarlord: this.state['checkbox' + j]
                                                };
                                            };

                                            this.joinTournament(tournamentDetails._id, user.token, bodyContent);
                                            Navigation.dismissModal(this.props.componentId);

                                        };
                                    }
                                )
                            }}
                        >
                            <Icon android="md-done" />
                            <Text style={{ color: 'white', textTransform: 'uppercase', fontSize: 15, textAlign: 'center', alignSelf: 'center', paddingBottom: 17 }}>DOŁĄCZ DO TURNIEJU</Text>
                        </Button>
                        <Button vertical onPress={() => {
                            Navigation.dismissModal(this.props.componentId);
                        }}>
                            <Icon android="cancel" />
                            <Text style={{ color: 'white', textTransform: 'uppercase', fontSize: 15, textAlign: 'center', alignSelf: 'center', paddingBottom: 17 }}>WRÓĆ</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    };
};

const mapStateToProps = state => {
    const tournamentDetails = state.tournaments.tournaments.filter(t => t._id === state.tournamentDetails.tournamentDetails)[0];

    return ({
        user: state.user.user,
        tournamentDetails,
        tournaments: state.tournaments.tournaments,
    })
};

const ConnectedJoinTournamentModal = connect(mapStateToProps)(JoinTournamentModal);

export default ConnectedJoinTournamentModal;