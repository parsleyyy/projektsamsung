import React, { Component } from 'react';
import { TouchableHighlight, Text, StyleSheet } from 'react-native';

class MenuOverlay extends Component {

    render() {

        return (
            <TouchableHighlight
                onPress={() => {
                    this.props.onToggleMenu()
                }}
                style={styles.overlay}>
                <Text></Text>
            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        position: 'absolute',
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
        padding: 10,
    }
})

export default MenuOverlay;