import React, { Component } from 'react';
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import MainScreen from '../Main/Main';
import ActiveGameScreen from '../ActiveGame/ActiveGame';
import { TextInput } from 'react-native';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';

class TabGenerator extends Component {
    render() {
        return (
            <Container>
                <Header hasTabs />
                <Tabs>
                    <Tab heading="All">
                        <MainScreen />
                    </Tab>
                    <Tab heading="Active">
                        <ActiveGameScreen />
                    </Tab>

                </Tabs>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    tournaments: state.tournaments,
    tournamentDetails: state.tournamentDetails,
    getUserLoading: state.user.getUserLoading,
});


const ConnectedTabGenerator = connect(mapStateToProps)(TabGenerator);
export default ConnectedTabGenerator;