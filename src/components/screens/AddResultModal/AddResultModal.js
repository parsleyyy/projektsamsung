import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
    Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Card, CardItem, Text,
    Accordion, View, ActionSheet, List, ListItem, Item, Input, Icon
} from 'native-base';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';
import DefaultHeader from '../UI/DefaultHeader';

class AddResultModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clicked: "",
            playerAResult: "",
            playerBResult: "",
        };
        Navigation.events().bindComponent(this);
        this.updateValue = this.updateValue.bind(this);
        this.addResult = this.addResult.bind(this);
    };

    updateValue(text, field) {
        this.setState({
            [field]: text,
        });
    };

    addResult() {
        const { dispatch, user, tournamentReportResult } = this.props;
        const { playerAResult, playerBResult } = this.state;
        dispatch(reduxActions.reportResultRequest(user.token, playerAResult, playerBResult, tournamentReportResult._id));
        Navigation.dismissModal(this.props.componentId);
    };

    render() {
        const ADD_RESULT_BUTTONS = [
            'Dodaj wyniki',
            'Wróć',
        ];
        const CANCEL_INDEX = 1;
        const { tournamentReportResult } = this.props;

        return (
            <Container>
                <DefaultHeader name='Dodaj wynik Twojej rozgrywki' />
                <Content padder>
                    <Card>
                        <CardItem key="playerA">
                            <View style={{ flex: 1 }}>
                                <Text style={{ textAlign: 'left', color: '#0D347D', fontSize: 20, fontWeight: 'bold' }}>{`Gracz ${tournamentReportResult.playerA.player && tournamentReportResult.playerA.player.firstName} ${tournamentReportResult.playerA.player && tournamentReportResult.playerA.player.lastName}`}</Text>
                            </View>
                        </CardItem>
                        <CardItem style={{
                            borderBottomWidth: 1,
                            borderBottomColor: '#d6d7da',
                            paddingBottom: 35,
                        }}>
                            <Item rounded>
                                <Input
                                    style={{ backgroundColor: '#D0DFEC', borderRadius: 50, }}
                                    placeholder={`   Wpisz wynik gracza ${tournamentReportResult.playerA.player && tournamentReportResult.playerA.player.firstName}...`}
                                    keyboardType='numeric'
                                    onChangeText={(text) => this.updateValue(text, 'playerAResult')}
                                    value={this.state.playerAResult}
                                />
                            </Item>
                        </CardItem>
                        <CardItem style={{ paddingTop: 35, }}>
                            <Item rounded >

                                <Input
                                    style={{ backgroundColor: '#ECDDD0', borderRadius: 50, textAlign: 'right' }}
                                    keyboardType='numeric'
                                    placeholder={`Wpisz wynik gracza ${tournamentReportResult.playerB.player && tournamentReportResult.playerB.player.firstName}...   `}
                                    onChangeText={(text) => this.updateValue(text, 'playerBResult')}
                                    value={this.state.playerBResult}
                                />
                            </Item>
                        </CardItem>
                        <CardItem key="playerB">
                            <View style={{ flex: 1 }}>
                                <Text style={{ textAlign: 'right', fontSize: 20, fontWeight: 'bold', color: '#604108' }}>{`Gracz ${tournamentReportResult.playerB.player && tournamentReportResult.playerB.player.firstName} ${tournamentReportResult.playerB.player && tournamentReportResult.playerB.player.lastName}`}</Text>
                            </View>
                        </CardItem>
                    </Card>
                </Content>
                <Footer>
                    <FooterTab>
                        <ActionSheet ref={(c) => { ActionSheet.actionsheetInstance = c; }} />
                        <Button
                            onPress={() => {
                                ActionSheet.show(
                                    {
                                        options: ADD_RESULT_BUTTONS,
                                        cancelButtonIndex: CANCEL_INDEX,
                                        title: "Czy na pewno?"
                                    },
                                    buttonIndex => {
                                        this.setState({ clicked: ADD_RESULT_BUTTONS[buttonIndex] });
                                        if (buttonIndex == 0) {
                                            this.addResult();
                                        };
                                    }
                                )
                            }}
                        >

                            {/* <Icon name="md-done-all" /> */}
                            <Text style={{ fontSize: 17, fontWeight: 'bold', paddingBottom: 0 }}>DODAJ WYNIKI</Text>


                        </Button>
                        <Button iconLeft
                            style={{ flex: 1 }}
                            onPress={() => {
                                Navigation.dismissModal(this.props.componentId);
                            }}>
                            {/* <Icon style={{ textAlign: 'left' }} name="md-close" /> */}
                            <Text style={{ fontSize: 17, fontWeight: 'bold', paddingBottom: 0, textAlign: 'center' }}>WRÓĆ</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    };
};

const mapStateToProps = (state, ownProps) => {
    const tournamentReportResult = state.activeGame.activeGame.match.filter(m => m._id === ownProps.getTournamentId)[0];

    return ({
        user: state.user.user,
        activeGame: state.activeGame.activeGame,
        tournamentReportResult,
    });
};

const ConnectedAddResultModal = connect(mapStateToProps)(AddResultModal);

export default ConnectedAddResultModal;