import React, { Component } from 'react';
import {
    Container, Left, Right, Header, Body, Title, Content, Footer, FooterTab, Button, Card, CardItem, Text, Item, Input,
    Accordion, View, ActionSheet, List, ListItem, Thumbnail
} from 'native-base';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import reduxActions from '../../../store/actions/login';
import { SetAuth, SetMain } from '../SetRoot';
import Icon from 'react-native-vector-icons/Ionicons';
import { Col, Row, Grid } from "react-native-easy-grid";

class SideMenu extends Component {
    
    constructor(props) {
        super(props);
        this.state = {}
        Navigation.events().bindComponent(this);
        this.handleLogOut = this.handleLogOut.bind(this);
    };

    handleLogOut() {
        const { dispatch } = this.props;
        dispatch(reduxActions.removeUser());
        SetAuth();
    };

    render() {
        const { email, firstName, lastName } = this.props.user;
        return (
            <Container>
                <Content padder>
                    <Grid style={{ paddingBottom: 25 }}>
                        <Col size={1}>
                            <Thumbnail large style={{ marginLeft: 10, marginTop: 15, }} source={require('../../../images/gamer.jpg')} />
                        </Col>
                        <Col size={3} style={{ paddingLeft: 10 }}>
                            <Row >
                                <Text style={{ fontSize: 25, marginLeft: 20, paddingTop: 22, }}>{`${firstName} ${lastName}`}</Text>
                            </Row>
                            <Row>
                                <Text style={{ fontSize: 20, marginLeft: 20, paddingBottom: 15 }}>{email}</Text>
                            </Row>
                        </Col>
                    </Grid>
                    <List>
                        <ListItem onPress={() => {
                            const { dispatch, user } = this.props;
                            dispatch(reduxActions.getTournamentRequest(user.token));

                            Navigation.mergeOptions('rightDrawer', {
                                sideMenu: {
                                    right: {
                                        visible: false,
                                    }
                                }
                            });
                        }}
                        >
                            <Grid>
                                <Col size={1} style={{}}><Icon style={{ fontSize: 40 }} name="md-home" /></Col>
                                <Col size={5} style={{}}><Text style={{ marginLeft: 10, marginTop: 12, textAlign: 'auto', alignSelf: 'flex-start' }}>Wróć</Text></Col>
                            </Grid>
                        </ListItem>
                        <ListItem onPress={() => {
                            Navigation.mergeOptions('rightDrawer', {
                                sideMenu: {
                                    right: {
                                        visible: false,
                                    }
                                }
                            });

                            Navigation.push('AppRoot', {
                                component: {
                                    name: 'games-app.ActiveGameScreen',
                                },
                            });
                        }}
                        >
                            <Grid>
                                <Col size={1}><Icon style={{ fontSize: 40 }} name="logo-game-controller-a" /></Col>
                                <Col size={5} style={{}}><Text style={{ marginLeft: 10, marginTop: 12, textAlign: 'auto', alignSelf: 'flex-start' }}>Aktywne gry</Text></Col>
                            </Grid>
                        </ListItem>
                        <ListItem onPress={() => {
                            Navigation.mergeOptions('rightDrawer', {
                                sideMenu: {
                                    right: {
                                        visible: false,
                                    },
                                },
                            });

                            Navigation.push('AppRoot', {
                                component: {
                                    name: 'games-app.GameHistoryScreen',
                                    options: {
                                        topBar: {
                                            title: {
                                                text: 'Wróć do listy turniejów',
                                            },
                                        },
                                    },
                                },
                            });
                        }}
                        >
                            <Grid>
                                <Col size={1}><Icon style={{ fontSize: 40 }} name="md-analytics" /></Col>
                                <Col size={5} style={{}}><Text style={{ marginLeft: 10, marginTop: 12, textAlign: 'auto', alignSelf: 'flex-start' }}>Historia moich gier</Text></Col>
                            </Grid>
                        </ListItem>
                        <ListItem onPress={() => {
                            const { dispatch, user } = this.props;
                            dispatch(reduxActions.rankingRequest(user.token));

                            Navigation.mergeOptions('rightDrawer', {
                                sideMenu: {
                                    right: {
                                        visible: false,
                                    }
                                }
                            });

                            Navigation.push('AppRoot', {
                                component: {
                                    name: 'games-app.RankingScreen',
                                },
                            });
                        }}
                        >
                            <Grid>
                                <Col size={1}><Icon style={{ fontSize: 40 }} name="md-trophy" /></Col>
                                <Col size={5} style={{}}><Text style={{ marginLeft: 10, marginTop: 12, textAlign: 'auto', alignSelf: 'flex-start' }}>Ranking</Text></Col>
                            </Grid>
                        </ListItem>
                        <ListItem onPress={this.handleLogOut}>
                            <Grid>
                                <Col size={1}><Icon style={{ fontSize: 40 }} name="md-log-out" /></Col>
                                <Col size={5} style={{}}><Text style={{ marginLeft: 10, marginTop: 12, textAlign: 'auto', alignSelf: 'flex-start' }}>Wyloguj się</Text></Col>
                            </Grid>
                        </ListItem>
                    </List>
                </Content>
            </Container >
        )
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    activeGame: state.activeGame.activeGame,
});

const ConnectedSideMenu = connect(mapStateToProps)(SideMenu);
export default ConnectedSideMenu;