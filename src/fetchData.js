const fetchData = {
    login: 'https://tournament--app.herokuapp.com/api/user/login',
    register: 'https://tournament--app.herokuapp.com/api/user/create',
    joinTournament: '',
    leaveTournament: '',
    getTournaments: 'https://tournament--app.herokuapp.com/api/tournament?&',
    activeGame: 'https://tournament--app.herokuapp.com/api/match?player=',
    gameHistory: 'https://tournament--app.herokuapp.com/api/match?player=',
    getRanking: 'https://tournament--app.herokuapp.com/api/ranking',
};

export default fetchData;