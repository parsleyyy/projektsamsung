import fetchData from '../../fetchData';

export default activeGame = (userId, token) => (
    fetch(fetchData.activeGame + userId + '&finished=false', {
        method: 'GET',
        headers: new Headers({
            "Authorization": `bearer ${token}`,//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Indhbm5hQHBvcGllbHVzemthLmNvbSIsImlkIjoiNWM5MzY3NjQ5MTEyMjFkMjYxZTMxNDY1IiwiaWF0IjoxNTUzNDkyMzQxLCJleHAiOjE1NTQwOTcxNDF9.ptl73Ox1mloDSR_9qRP6Q6cB1thAiJTvLjOK6yGVMTg"
        }),
    }).then(response => response.json())
        .then(res => {
            console.log('TUTAJ ACTIVEGAME!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            console.log(res);
            return res;
        }, err => err)
        .catch(error => console.log('Error activeGame: ', error))
);

