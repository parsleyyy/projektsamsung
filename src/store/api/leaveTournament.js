import { Alert } from 'react-native';

const leaveTournament = (tournamentId, userToken) => (
  fetch(`https://tournament--app.herokuapp.com/api/tournament/${  tournamentId  }/unregister`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userToken,
    }),
  }).then(res => res.json())
    .then((response) => {
      Alert.alert(
        response.message,
      );
      return response;
    })
    .catch(err => err)
);

export default leaveTournament;
