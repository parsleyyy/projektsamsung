import fetchData from '../../fetchData';

export default login = data => (
    fetch(fetchData.login, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
    }).then(res => res.json())
        .then(response => {
            const { _id, email, token, message, lastName, firstName } = response.token;
            if (message) return 'OLABOGA';
            if (_id && email && token) {
                const user = {
                    id: _id,
                    email,
                    token,
                    lastName,
                    firstName,
                };
                console.log('Success: ', user);
                return user;
            }
        }, err => err)
        .catch(error => { return error; })
);