import fetchData from '../../fetchData';

const showTournaments = token => fetch(fetchData.getTournaments, {
  method: 'GET',
  headers: new Headers({
    Authorization: `bearer ${token}`,
    'Content-Type': 'application/json',
  }),
})
  .then(response => response.json())
  .then(res => res, err => err)
  .catch(error => error);

export default showTournaments;
