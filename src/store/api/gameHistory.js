import fetchData from '../../fetchData';

export default gameHistory = (userId, token) => (
  fetch(`${fetchData.gameHistory + userId }&finished=true`, {
    method: 'GET',
    headers: new Headers({
      Authorization: `bearer ${token}`,
    }),
  }).then(response => response.json())
    .then(res => res, err => err)
    .catch(error => console.log('Error gameHistory: ', error))
);
