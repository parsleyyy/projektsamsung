import fetchData from '../../fetchData';

const getRanking = userToken =>
    fetch(fetchData.getRanking, {
        method: 'GET',
        headers: new Headers({
            "Authorization": `bearer ${userToken}`,
            'Content-Type': 'application/json',
        }),
    })
        .then(response => response.json())
        .then(res => {
            console.log('WYSWIETLENIE RANKINGUUUUUUUUUUU');
            console.log(res);
            return res;
        }, err => err)
        .catch(error => console.log('Error w wyswitleniu rankingu: ', JSON.stringify(error)));

export default getRanking;

