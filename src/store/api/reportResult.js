import { Alert } from 'react-native';

export default reportResult = (token, scoreA, scoreB, matchId) => (
  fetch(`https://tournament--app.herokuapp.com/api/match/${ matchId }/finish`, {
    method: 'POST',
    body: JSON.stringify({ scoreA, scoreB }),
    headers: new Headers({
      'Authorization': `bearer ${token}`,
      'Content-Type': 'application/json',
    }),
  }).then(response => response.json())
    .then((res) => {
      if (res.match) {
        Alert.alert(
          'Dodano wynik.',
        );
      } else if (res.message) {
        Alert.alert(
          res.message,
        );
      }
      return res;
    }, err => err)
    .catch(error => console.log(JSON.stringify(error.message)))
);
