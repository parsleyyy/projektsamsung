import { Alert } from 'react-native';

const joinTournament = (tournamentId, userToken, bodyContent) => (
  fetch(`https://tournament--app.herokuapp.com/api/tournament/${  tournamentId  }/register`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + userToken,
    }),
    body: bodyContent,
  }).then(res => res.json())
    .then((response) => {
      Alert.alert(
        response.message,
      );
      return response;
    })
    .catch(err => err)
);

export default joinTournament;
