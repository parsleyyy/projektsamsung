import AsyncStorage from '@react-native-community/async-storage';

const getUserData = () =>
    AsyncStorage.getItem('user')
        .then((data) => {
            try {
                console.log(data);
                console.log(JSON.parse(data));
                return JSON.parse(data);
            }
            catch {
                return {};
            }
        })
        .catch((err) => err);

const setUserData = (data) =>
    AsyncStorage.setItem('user', JSON.stringify(data))
        .then((data) => data)
        .catch((err) => err);

const removeUserData = () =>
    AsyncStorage.removeItem('user')
        .then((data) => data)
        .catch((err) => err);

const getTournamentsData = () =>
    AsyncStorage.getItem('tournaments')
        .then((data) => {
            try {
                return JSON.parse(data);
            }
            catch {
                return {};
            }
        }, err => err);

const setTournamentsData = (tournaments) =>
    AsyncStorage.setItem('tournamnets', JSON.stringify(tournaments));


// const removeTournamentsData = () =>
//     AsyncStorage.removeItem

export default { getUserData, setUserData, removeUserData, getTournamentsData, setTournamentsData };
