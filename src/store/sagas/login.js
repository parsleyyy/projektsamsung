import {
  take,
  put,
  takeEvery,
  call,
  all,
  race,
  select,
} from 'redux-saga/effects';
import actionTypes from '../actions/actionTypes';
import actions from '../actions/login';
import login from '../api/login';
import showTournaments from '../api/showTournaments';
import joinTournament from '../api/joinTournament';
import leaveTournament from '../api/leaveTournament';
import reportResult from '../api/reportResult';
import activeGame from '../api/activeGame';
import gameHistory from '../api/gameHistory';
import getRanking from '../api/getRanking';
import SetMain from '../../components/screens/SetRoot/SetMain';
import SetAuth from '../../components/screens/SetRoot/SetAuth';
import storage from '../asyncStorage/asyncStorage';

export function* loginAsync(action) {
  const winner = yield race({
    user: call(login, action.payload),
    logout: take(actionTypes.REMOVE_USER),
  });

  if (winner.user) {
    if (winner.user.message || winner.user instanceof Error) {
      yield put({
        type: actionTypes.GET_USER_FAILURE,
        getUserError: winner.user,
      });
    } else {
      yield put({
        type: actionTypes.GET_USER_SUCCESS,
        user: winner.user,
      });
      yield call(storage.setUserData, winner.user);
      yield call(SetMain);
    }
  } else {
    yield put({
      type: actionTypes.GET_USER_FAILURE,
    });
  }
}

export function* logoutAsync() {
  yield call(storage.removeUserData);
  yield call(SetAuth);
}

export function* getTournamentsAsync(action) {
  const response = yield call(showTournaments, action.token);
  if (response instanceof Error) {
    yield put({
      type: actionTypes.GET_TOURNAMENT_FAILURE,
      error: response,
    });
    const asResponse = yield call(storage.getTournamentsData);
    if (asResponse) {
      yield put({
        type: actionTypes.GET_TOURNAMENT_SUCCESS,
        tournaments: asResponse,
      });
    }
  } else {
    yield call(storage.setTournamentsData, response);
    yield put({
      type: actionTypes.GET_TOURNAMENT_SUCCESS,
      tournaments: response,
    });
  }
}

export function* joinTournamentAsync(action) {
  const response = yield call(joinTournament, action.tournamentId, action.userToken);

  if (response instanceof Error) {
    yield put({
      type: actionTypes.JOIN_TOURNAMENT_FAILURE,
      error: response,
    });
  } else {
    yield put({
      type: actionTypes.JOIN_TOURNAMENT_SUCCESS,
      response,
    });
    yield put({
      type: actionTypes.GET_TOURNAMENT_REQUEST,
      token: action.userToken,
    });
  }
}

export function* leaveTournamentAsync(action) {
  const response = yield call(leaveTournament, action.tournamentId, action.userToken);

  if (response instanceof Error) {
    yield put({
      type: actionTypes.LEAVE_TOURNAMENT_FAILURE,
      error: response,
    });
  } else {
    yield put({
      type: actionTypes.LEAVE_TOURNAMENT_SUCCESS,
      response,
    });
    yield put({
      type: actionTypes.GET_TOURNAMENT_REQUEST,
      token: action.userToken,
    });
  }
}

export function* activeGameAsync(action) {
  const response = yield call(activeGame, action.userId, action.token);

  if (response instanceof Error) {
    yield put({
      type: actionTypes.ACTIVEGAME_FAILURE,
      activeGameError: response,
    });
  } else {
    yield put({
      type: actionTypes.ACTIVEGAME_SUCCESS,
      activeGame: response,
    });
  }
}

export function* gameHistoryAsync(action) {
  const response = yield call(gameHistory, action.userId, action.token);

  if (response instanceof Error) {
    yield put({
      type: actionTypes.GAMEHISTORY_FAILURE,
      gameHistoryError: response,
    });
  } else {
    yield put({
      type: actionTypes.GAMEHISTORY_SUCCESS,
      gameHistory: response,
    });
  }
}

export function* reportResultAsync(action) {
  const res = yield call(reportResult, action.token, action.scoreA, action.scoreB, action.matchId);
  if (res instanceof Error || res.message) {
    yield put({
      type: actionTypes.REPORT_RESULT_FAILURE,
      error: res,
    });
  } else {
    yield put({
      type: actionTypes.REPORT_RESULT_SUCCESS,
      reportResult: res,
    });

    const id = yield select(state => state.user.user.id);
    yield put(
      actions.activeGameRequest(id, action.token),
    );
  }
}

export function* rankingAsync(action) {
  const res = yield call(getRanking, action.userToken);

  if (res instanceof Error) {
    yield put({
      type: actionTypes.RANKING_FAILURE,
      error: res,
    });
  } else {
    yield put({
      type: actionTypes.RANKING_SUCCESS,
      ranking: res,
    });
  }
}

export function* rootSaga() {
  yield all([
    takeEvery(actionTypes.GET_USER_REQUEST, loginAsync),
    takeEvery(actionTypes.REMOVE_USER, logoutAsync),
    takeEvery(actionTypes.GET_TOURNAMENT_REQUEST, getTournamentsAsync),
    takeEvery(actionTypes.JOIN_TOURNAMENT_REQUEST, joinTournamentAsync),
    takeEvery(actionTypes.LEAVE_TOURNAMENT_REQUEST, leaveTournamentAsync),
    takeEvery(actionTypes.ACTIVEGAME_REQUEST, activeGameAsync),
    takeEvery(actionTypes.REPORT_RESULT_REQUEST, reportResultAsync),
    takeEvery(actionTypes.GAMEHISTORY_REQUEST, gameHistoryAsync),
    takeEvery(actionTypes.RANKING_REQUEST, rankingAsync),
  ]);
}
