import actionTypes from '../actions/actionTypes';
import reducerReportResult from './reportResult';

describe('report result reducer test suite', () => {
  const initialStateReportResult = {
    reportResult: {},
    reportResultLoading: false,
    error: null,
  };

  it('should return initial state when no action is given', () => {
    expect(reducerReportResult()).toEqual(initialStateReportResult);
  });

  it('should set loading and reset error on request action', () => {
    expect(reducerReportResult({
      ...initialStateReportResult,
      error: new Error('fake err'),
    }, {
      type: actionTypes.REPORT_RESULT_REQUEST,
    })).toEqual({
      reportResultLoading: true,
      error: null,
      reportResult: {},
    });
  });

  it('should set result and reset loading', () => {
    expect(reducerReportResult({
      ...initialStateReportResult,
      reportResultLoading: true,
    }, {
      type: actionTypes.REPORT_RESULT_SUCCESS,
      reportResult: { fake: 'id' },
    })).toEqual({
      reportResultLoading: false,
      reportResult: { fake: 'id' },
      error: null,
    });
  });

  it('should set error, reset loading and result', () => {
    expect(reducerReportResult({
      ...initialStateReportResult,
      reportResultLoading: false,
      reportResult: { fake: 'id' },
    }, {
      type: actionTypes.REPORT_RESULT_FAILURE,
      error: new Error('fake err'),
    })).toEqual({
      reportResultLoading: false,
      reportResult: {},
      error: new Error('fake err'),
    });
  });
});
