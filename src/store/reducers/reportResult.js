import actionTypes from '../actions/actionTypes';

const initialStateReportResult = {
  reportResult: {},
  reportResultLoading: false,
  error: null,
};

const reducerReportResult = (state = initialStateReportResult, action = {}) => {
  switch (action.type) {
    case actionTypes.REPORT_RESULT_REQUEST:
      return { ...state, reportResultLoading: true, error: null };
    case actionTypes.REPORT_RESULT_SUCCESS:
      return { ...state, reportResultLoading: false, reportResult: action.reportResult };
    case actionTypes.REPORT_RESULT_FAILURE:
      return { ...state, reportResult: {}, reportResultLoading: false, error: action.error };
    default:
      return state;
  }
};

export default reducerReportResult;
