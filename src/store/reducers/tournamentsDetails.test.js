import actionTypes from '../actions/actionTypes';
import reducerTournamentDetails from './tournamentsDetails';

describe('tournamentDetails test suite', () => {
  const initialStateTournamentDetails = {
    tournamentDetails: {},
  };

  it('should return initial state when no action is given', () => {
    expect(reducerTournamentDetails()).toEqual(initialStateTournamentDetails);
  });

  it('should set tournament details', () => {
    expect(reducerTournamentDetails({
      ...initialStateTournamentDetails,
    }, {
      type: actionTypes.GET_TOURNAMENT_DETAILS,
      tournamentDetails: { fake: 'id' },
    })).toEqual({
      tournamentDetails: { fake: 'id' },
    });
  });
});
