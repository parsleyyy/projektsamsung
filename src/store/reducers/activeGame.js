import actionTypes from '../actions/actionTypes';

const initialStateActiveGame = {
  activeGame: { match: [{}] },
  error: null,
  activeGameLoading: false,
};

const reducerActiveGame = (state = initialStateActiveGame, action = {}) => {
  switch (action.type) {
    case actionTypes.ACTIVEGAME_REQUEST:
      return { ...state, activeGameLoading: true, error: null };
    case actionTypes.ACTIVEGAME_SUCCESS:
      return { ...state, activeGame: action.activeGame, activeGameLoading: false };
    case actionTypes.ACTIVEGAME_FAILURE:
      return {
        ...state, activeGame: { match: [{}] }, activeGameLoading: false, error: action.error,
      };
    default:
      return state;
  }
};

export default reducerActiveGame;
