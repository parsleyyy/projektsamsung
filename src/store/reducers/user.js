import actionTypes from '../actions/actionTypes';

const initialStateUser = {
  user: {},
  getUserLoading: false,
  error: null,
};

const reducerUser = (state = initialStateUser, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_USER:
      return { ...state, user: action.user };
    case actionTypes.REMOVE_USER:
      return { ...state, user: {} };
    case actionTypes.GET_USER_FAILURE:
      return {
        ...state, user: {}, getUserLoading: false, error: action.error,
      };
    case actionTypes.GET_USER_SUCCESS:
      return { ...state, user: action.user, getUserLoading: false };
    case actionTypes.GET_USER_REQUEST:
      return {
        ...state, payload: action.payload, getUserLoading: true, error: null,
      };
    default:
      return state;
  }
};

export default reducerUser;
