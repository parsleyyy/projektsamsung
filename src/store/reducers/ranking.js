import actionTypes from '../actions/actionTypes';

const initialStateRanking = {
  ranking: [],
  error: null,
  rankingLoading: false,
};

const reducerRanking = (state = initialStateRanking, action = {}) => {
  switch (action.type) {
    case actionTypes.RANKING_REQUEST:
      return { ...state, rankingLoading: true, error: null };
    case actionTypes.RANKING_SUCCESS:
      return { ...state, rankingLoading: false, ranking: action.ranking };
    case actionTypes.RANKING_FAILURE:
      return { ...state, ranking: [], rankingLoading: false, error: action.error };
    default:
      return state;
  }
};

export default reducerRanking;
