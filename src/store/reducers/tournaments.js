import actionTypes from '../actions/actionTypes';

const initialStateTournaments = {
  tournaments: [],
  getTournamentLoading: false,
  joinTournamentLoading: false,
  leaveTournamentLoading: false,
  response: '',
  error: null,
};

const reducerTournaments = (state = initialStateTournaments, action = {}) => {
  switch (action.type) {
    case actionTypes.GET_TOURNAMENT_FAILURE:
      return {
        ...state, tournaments: [], error: action.error, getTournamentLoading: false,
      };
    case actionTypes.GET_TOURNAMENT_REQUEST:
      return { ...state, getTournamentLoading: true, error: null };
    case actionTypes.GET_TOURNAMENT_SUCCESS:
      return { ...state, tournaments: action.tournaments, getTournamentLoading: false };

    case actionTypes.JOIN_TOURNAMENT_FAILURE:
      return {
        ...state, joinTournamentLoading: false, response: '', error: action.error,
      };
    case actionTypes.JOIN_TOURNAMENT_SUCCESS:
      return { ...state, joinTournamentLoading: false, response: action.response };
    case actionTypes.JOIN_TOURNAMENT_REQUEST:
      return { ...state, joinTournamentLoading: true, error: null };

    case actionTypes.LEAVE_TOURNAMENT_REQUEST:
      return { ...state, leaveTournamentLoading: true, error: null };
    case actionTypes.LEAVE_TOURNAMENT_SUCCESS:
      return { ...state, leaveTournamentLoading: false, response: action.response };
    case actionTypes.LEAVE_TOURNAMENT_FAILURE:
      return {
        ...state, leaveTournamentLoading: false, response: '', error: action.error,
      };

    default:
      return state;
  }
};

export default reducerTournaments;
