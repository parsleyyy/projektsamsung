import { combineReducers, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import { rootSaga } from '../sagas/login';

import reducerActiveGame from './activeGame';
import reducerUser from './user';
import reducerFilters from './filters';
import reducerTournaments from './tournaments';
import reducerTournamentDetails from './tournamentsDetails';
import reducerGameHistory from './gameHistory';
import reducerReportResult from './reportResult';
import reducerRanking from './ranking';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger();

const rootReducer = combineReducers({
  user: reducerUser,
  tournaments: reducerTournaments,
  tournamentDetails: reducerTournamentDetails,
  activeGame: reducerActiveGame,
  reportResult: reducerReportResult,
  gameHistory: reducerGameHistory,
  ranking: reducerRanking,
  filters: reducerFilters,
});

export default createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware, loggerMiddleware),
);

sagaMiddleware.run(rootSaga);
