import actionTypes from '../actions/actionTypes';
import reducerRanking from './ranking';

describe('Ranking reducer test suite', () => {
  const initialStateRanking = {
    ranking: [],
    error: null,
    rankingLoading: false,
  };

  it('should return initial state when no action is given', () => {
    expect(reducerRanking()).toEqual(initialStateRanking);
  });

  it('should set loading and reset error on request action', () => {
    expect(reducerRanking({
      ...initialStateRanking,
      error: new Error('fake err'),
    }, {
      type: actionTypes.RANKING_REQUEST,
    })).toEqual({
      rankingLoading: true,
      error: null,
      ranking: [],
    });
  });

  it('should set ranking and reset loading', () => {
    expect(reducerRanking({
      ...initialStateRanking,
      rankingLoading: true,
    }, {
      type: actionTypes.RANKING_SUCCESS,
      ranking: { fake: 'id' },
    })).toEqual({
      rankingLoading: false,
      error: null,
      ranking: { fake: 'id' },
    });
  });

  it('should set error and reset loading and ranking', () => {
    expect(reducerRanking({
      ...initialStateRanking,
      rankingLoading: true,
      ranking: { fake: 'id' },
    }, {
      type: actionTypes.RANKING_FAILURE,
      error: new Error('fake error'),
    })).toEqual({
      rankingLoading: false,
      error: new Error('fake error'),
      ranking: [],
    });
  });
});
