import actionTypes from '../actions/actionTypes';

const initialStateGameHistory = {
  gameHistory: { match: [{}] },
  error: null,
  gameHistoryLoading: false,
};

const reducerGameHistory = (state = initialStateGameHistory, action = {}) => {
  switch (action.type) {
    case actionTypes.GAMEHISTORY_REQUEST:
      return { ...state, gameHistoryLoading: true, error: null };
    case actionTypes.GAMEHISTORY_SUCCESS:
      return { ...state, gameHistory: action.gameHistory, gameHistoryLoading: false };
    case actionTypes.GAMEHISTORY_FAILURE:
      return {
        ...state, gameHistory: { match: [{}] }, gameHistoryLoading: false, error: action.error,
      };
    default:
      return state;
  }
};

export default reducerGameHistory;
