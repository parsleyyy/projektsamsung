import actionTypes from '../actions/actionTypes';
import reducerUser from './user';

describe('user reducer test suite', () => {
  const initialStateUser = {
    user: {},
    getUserLoading: false,
    error: null,
  };

  it('should return initial state when no action is given', () => {
    expect(reducerUser()).toEqual(initialStateUser);
  });

  it('should set loading and reset error on request action', () => {
    expect(reducerUser({
      ...initialStateUser,
      error: new Error('fake err'),
    }, {
      type: actionTypes.GET_USER_REQUEST,
    })).toEqual({
      getUserLoading: true,
      error: null,
      user: {},
    });
  });

  it('should get user and reset loading', () => {
    expect(reducerUser({
      ...initialStateUser,
      getUserLoading: true,
    }, {
      type: actionTypes.GET_USER_SUCCESS,
      user: { fake: 'id' },
    })).toEqual({
      getUserLoading: false,
      error: null,
      user: { fake: 'id' },
    });
  });

  it('should set error and reset loading and user', () => {
    expect(reducerUser({
      ...initialStateUser,
      getUserLoading: true,
      user: { fake: 'id' },
    }, {
      type: actionTypes.GET_USER_FAILURE,
      error: new Error('fake err'),
    })).toEqual({
      getUserLoading: false,
      error: new Error('fake err'),
      user: {},
    });
  });

  it('should set user', () => {
    expect(reducerUser({
      ...initialStateUser,
    }, {
      type: actionTypes.SET_USER,
      user: { fake: 'id' },
    })).toEqual({
      user: { fake: 'id' },
      error: null,
      getUserLoading: false,
    });
  });

  it('should remove user', () => {
    expect(reducerUser({
      ...initialStateUser,
      user: { fake: 'id' },
    }, {
      type: actionTypes.REMOVE_USER,
    })).toEqual({
      user: {},
      error: null,
      getUserLoading: false,
    });
  });
});
