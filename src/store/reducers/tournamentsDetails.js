import actionTypes from '../actions/actionTypes';

const initialStateTournamentDetails = {
  tournamentDetails: {},
};

const reducerTournamentDetails = (state = initialStateTournamentDetails, action = {}) => {
  switch (action.type) {
    case actionTypes.GET_TOURNAMENT_DETAILS:
      return { ...state, tournamentDetails: action.tournamentDetails };
    default:
      return state;
  }
};

export default reducerTournamentDetails;
