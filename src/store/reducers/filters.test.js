import actionTypes from '../actions/actionTypes';
import reducerFilters from './filters';

describe('fiters reducer test suite', () => {
  const initialStateFilters = {
    freePlaces: false,
    name: '',
    city: '',
    dateFrom: undefined,
    dateTo: undefined,
    rank: 'Dowolna',
  };

  it('should return initial state whrn no action is given', () => {
    expect(reducerFilters()).toEqual(initialStateFilters);
  });

  it('should set filters', () => {
    expect(reducerFilters({
      ...initialStateFilters,
    }, {
      type: actionTypes.SET_FILTERS,
      key: 'key',
      value: 'value',
    })).toEqual({
      ...initialStateFilters,
      key: 'value',
    });
  });

  it('should remove filters', () => {
    expect(reducerFilters({
      ...initialStateFilters,
    }, {
      type: actionTypes.REMOVE_FILTERS,
    })).toEqual({
      freePlaces: false,
      name: '',
      city: '',
      dateFrom: undefined,
      dateTo: undefined,
      rank: 'Dowolna',
    });
  });
});
