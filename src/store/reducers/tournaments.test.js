import actionTypes from '../actions/actionTypes';
import reducerTournaments from './tournaments';

describe('tournaments reducer test suite', () => {
  const initialStateTournaments = {
    tournaments: [],
    getTournamentLoading: false,
    joinTournamentLoading: false,
    leaveTournamentLoading: false,
    response: '',
    error: null,
  };

  it('should return initial state when no action is given', () => {
    expect(reducerTournaments()).toEqual(initialStateTournaments);
  });

  it('should set loading and reset error on request action GET', () => {
    expect(reducerTournaments({
      ...initialStateTournaments,
      error: new Error('fake err'),
    }, {
      type: actionTypes.GET_TOURNAMENT_REQUEST,
    })).toEqual({
      getTournamentLoading: true,
      error: null,
      tournaments: [],
      joinTournamentLoading: false,
      leaveTournamentLoading: false,
      response: '',
    });
  });

  it('should get tournaments and reset loading GET', () => {
    expect(reducerTournaments({
      ...initialStateTournaments,
      getTournamentLoading: true,
    }, {
      type: actionTypes.GET_TOURNAMENT_SUCCESS,
      tournaments: { fake: 'id' },
    })).toEqual({
      getTournamentLoading: false,
      tournaments: { fake: 'id' },
      joinTournamentLoading: false,
      leaveTournamentLoading: false,
      response: '',
      error: null,
    });
  });

  it('should set error and reset loading and tournaments', () => {
    expect(reducerTournaments({
      ...initialStateTournaments,
      getTournamentLoading: true,
      tournaments: { fake: 'id' },
    }, {
      type: actionTypes.GET_TOURNAMENT_FAILURE,
      error: new Error('fake err'),
    })).toEqual({
      getTournamentLoading: false,
      error: new Error('fake err'),
      tournaments: [],
      joinTournamentLoading: false,
      leaveTournamentLoading: false,
      response: '',
    });
  });

  it('should set join loading', () => {
    expect(reducerTournaments({
      ...initialStateTournaments,
      joinTournamentLoading: false,
    }, {
      type: actionTypes.JOIN_TOURNAMENT_REQUEST,
    })).toEqual({
      joinTournamentLoading: true,
      error: null,
      tournaments: [],
      getTournamentLoading: false,
      leaveTournamentLoading: false,
      response: '',
    });
  });

  it('should set leave loading', () => {
    expect(reducerTournaments({
      ...initialStateTournaments,
      leaveTournamentLoading: false,
    }, {
      type: actionTypes.LEAVE_TOURNAMENT_REQUEST,
    })).toEqual({
      leaveTournamentLoading: true,
      error: null,
      tournaments: [],
      getTournamentLoading: false,
      joinTournamentLoading: false,
      response: '',
    });
  });

  it('should reset loading and set response JOIN', () => {
    expect(reducerTournaments({
      ...initialStateTournaments,
      joinTournamentLoading: true,
    }, {
      type: actionTypes.JOIN_TOURNAMENT_SUCCESS,
      response: { fake: 'id' },
    })).toEqual({
      leaveTournamentLoading: false,
      error: null,
      tournaments: [],
      getTournamentLoading: false,
      joinTournamentLoading: false,
      response: { fake: 'id' },
    });
  });

  it('should reset loading and set response LEAVE', () => {
    expect(reducerTournaments({
      ...initialStateTournaments,
      leaveTournamentLoading: true,
    }, {
      type: actionTypes.LEAVE_TOURNAMENT_SUCCESS,
      response: { fake: 'id' },
    })).toEqual({
      leaveTournamentLoading: false,
      error: null,
      tournaments: [],
      getTournamentLoading: false,
      joinTournamentLoading: false,
      response: { fake: 'id' },
    });
  });

  it('should set error, reset loading and response JOIN', () => {
    expect(reducerTournaments({
      ...initialStateTournaments,
      joinTournamentLoading: true,
      response: { fake: 'id' },
    }, {
      type: actionTypes.JOIN_TOURNAMENT_FAILURE,
      error: new Error('fake err'),
    })).toEqual({
      leaveTournamentLoading: false,
      error: new Error('fake err'),
      tournaments: [],
      getTournamentLoading: false,
      joinTournamentLoading: false,
      response: '',
    });
  });
});
