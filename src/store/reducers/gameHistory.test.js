import actionTypes from '../actions/actionTypes';
import reducerGameHistory from './gameHistory';

describe('game history reducer test suite', () => {
  const initialStateGameHistory = {
    gameHistory: { match: [{}] },
    error: null,
    gameHistoryLoading: false,
  };

  it('should return initial state when no action is given', () => {
    expect(reducerGameHistory()).toEqual(initialStateGameHistory);
  });

  it('should set loading and reset error on request action', () => {
    expect(reducerGameHistory({
      ...initialStateGameHistory,
      error: new Error('fake err'),
    }, {
      type: actionTypes.GAMEHISTORY_REQUEST,
    })).toEqual({
      gameHistoryLoading: true,
      error: null,
      gameHistory: { match: [{}] },
    });
  });

  it('should set gameHistory and reset loading', () => {
    expect(reducerGameHistory({
      ...initialStateGameHistory,
      gameHistoryLoading: true,
    }, {
      type: actionTypes.GAMEHISTORY_SUCCESS,
      gameHistory: { fake: 'id' },
    })).toEqual({
      gameHistoryLoading: false,
      error: null,
      gameHistory: { fake: 'id' },
    });
  });

  it('should set error and reset loading and gameHistory', () => {
    expect(reducerGameHistory({
      ...initialStateGameHistory,
      gameHistoryLoading: true,
      gameHistory: { fake: 'id' },
    }, {
      type: actionTypes.GAMEHISTORY_FAILURE,
      error: new Error('fake error'),
    })).toEqual({
      gameHistoryLoading: false,
      error: new Error('fake error'),
      gameHistory: { match: [{}] },
    });
  });
});
