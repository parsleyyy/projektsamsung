import actionTypes from '../actions/actionTypes';
import reducerActiveGame from './activeGame';
// import AsyncStorage from '@react-native-community/async-storage';

// beforeAll(() => {
//     jest.mock('@react-native-community/async-storage');
// });

describe('active game reducer test suite', () => {
  const initialStateActiveGame = {
    activeGame: { match: [{}] },
    activeGameLoading: false,
    error: null,
  };

  it('should return initial state when no action is given', () => {
    expect(reducerActiveGame()).toEqual(initialStateActiveGame);
  });

  it('should set loading and reset error on request action', () => {
    expect(reducerActiveGame({
      ...initialStateActiveGame,
      error: new Error('fake err'),
    }, {
      type: actionTypes.ACTIVEGAME_REQUEST,
    })).toEqual({
      activeGameLoading: true,
      error: null,
      activeGame: { match: [{}] },
    });
  });

  it('should set activeGame and reset loading', () => {
    expect(reducerActiveGame({
      ...initialStateActiveGame,
      activeGameLoading: true,
    }, {
      type: actionTypes.ACTIVEGAME_SUCCESS,
      activeGame: { fake: 'id' },
    })).toEqual({
      activeGameLoading: false,
      error: null,
      activeGame: { fake: 'id' },
    });
  });

  it('should set error and reset loading and activeGame', () => {
    expect(reducerActiveGame({
      ...initialStateActiveGame,
      activeGameLoading: true,
      activeGame: { fake: 'id' },
    }, {
      type: actionTypes.ACTIVEGAME_FAILURE,
      error: new Error('fake error'),
    })).toEqual({
      activeGameLoading: false,
      error: new Error('fake error'),
      activeGame: { match: [{}] },
    });
  });
});
