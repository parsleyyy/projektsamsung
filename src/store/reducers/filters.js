import actionTypes from '../actions/actionTypes';

const initialStateFilters = {
  freePlaces: false,
  name: '',
  city: '',
  dateFrom: undefined,
  dateTo: undefined,
  rank: 'Dowolna',
};

const reducerFilters = (state = initialStateFilters, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_FILTERS:
      return {
        ...state,
        [action.key]: action.value,
      };
    case actionTypes.REMOVE_FILTERS:
      return {
        ...state,
        freePlaces: false,
        name: '',
        city: '',
        dateFrom: undefined,
        dateTo: undefined,
        rank: 'Dowolna',
      };
    default:
      return state;
  }
};

export default reducerFilters;
