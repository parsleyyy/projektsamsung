import actionTypes from './actionTypes';

// LOGOWANIE ?
const getUserRequest = data => ({
  type: actionTypes.GET_USER_REQUEST,
  payload: data,
});

const getUserSuccess = () => ({
  type: actionTypes.GET_USER_SUCCESS,
});

const getUserFailure = () => ({
  type: actionTypes.GET_USER_FAILURE,
});

const getUserLoading = () => ({
  type: actionTypes.GET_USER_LOADING,
});


// const getUser = user => ({
//     type: actionTypes.GET_USER,
//     user
// });

// WTF
const setUser = user => ({
  type: actionTypes.SET_USER,
  user,
});

// WTF
const removeUser = () => ({
  type: actionTypes.REMOVE_USER,
});

// wyświetlanie turniejów w mainscreen
const getTournamentRequest = token => ({
  type: actionTypes.GET_TOURNAMENT_REQUEST,
  token,
});

const getTournamentSuccess = () => ({
  type: actionTypes.GET_TOURNAMENT_SUCCESS,
});

const getTournamentFailure = () => ({
  type: actionTypes.GET_TOURNAMENT_FAILURE,
});

const getTournamentLoading = () => ({
  type: actionTypes.GET_TOURNAMENT_LOADING,
});

const setFilters = (key, value) => ({
  type: actionTypes.SET_FILTERS,
  key,
  value,
});

const removeFilters = () => ({
  type: actionTypes.REMOVE_FILTERS,
});


// rejestracja na turniej - JOIN W TOURNAMNETDESCTIPTION
const joinTournamentRequest = (tournamentId, userToken, keyword, withWarlord) => ({
  type: actionTypes.JOIN_TOURNAMENT_REQUEST,
  tournamentId,
  userToken,
  keyword,
  withWarlord,
});

const joinTournamentFailure = () => ({
  type: actionTypes.JOIN_TOURNAMENT_FAILURE,
});

const joinTournamentSuccess = () => ({
  type: actionTypes.JOIN_TOURNAMENT_SUCCESS,
});

const joinTournamentLoading = () => ({
  type: actionTypes.JOIN_TOURNAMENT_LOADING,
});

// odrejestrowywanie sie z turnieju
const leaveTournamentRequest = (tournamentId, userToken) => ({
  type: actionTypes.LEAVE_TOURNAMENT_REQUEST,
  tournamentId,
  userToken,
});

const leaveTournamentFailure = () => ({
  type: actionTypes.LEAVE_TOURNAMENT_FAILURE,
});

const leaveTournamentSuccess = () => ({
  type: actionTypes.LEAVE_TOURNAMENT_SUCCESS,
});

const leaveTournamentLoading = () => ({
  type: actionTypes.LEAVE_TOURNAMENT_LOADING,
});

// WYŚWIETLANIE DETALI JEDNEGO Z TURNIEJÓW - SCREEN TOURNAMENTDESCRIPTION
const getTournamentDetails = tournamentDetails => ({
  type: actionTypes.GET_TOURNAMENT_DETAILS,
  tournamentDetails,
});

const getTournamentDetailsLoading = () => ({
  type: actionTypes.GET_TOURNAMENT_DETAILS_LOADING,
});

const activeGameRequest = (userId, token) => ({
  type: actionTypes.ACTIVEGAME_REQUEST,
  token,
  userId,
});

const activeGameSuccess = () => ({
  type: actionTypes.ACTIVEGAME_SUCCESS,
});

const activeGameFailure = () => ({
  type: actionTypes.ACTIVEGAME_FAILURE,
});

const activeGameLoading = () => ({
  type: actionTypes.ACTIVEGAME_LOADING,
});

const gameHistoryRequest = (userId, token) => ({
  type: actionTypes.GAMEHISTORY_REQUEST,
  token,
  userId,
});

const gameHistorySuccess = () => ({
  type: actionTypes.GAMEHISTORY_SUCCESS,
});

const gameHistoryFailure = () => ({
  type: actionTypes.GAMEHISTORY_FAILURE,
});

const gameHistoryLoading = () => ({
  type: actionTypes.GAMEHISTORY_LOADING,
});

const reportResultRequest = (token, scoreA, scoreB, matchId) => ({
  type: actionTypes.REPORT_RESULT_REQUEST,
  token,
  scoreA,
  scoreB,
  matchId,
});

const reportResultSuccess = () => ({
  type: actionTypes.REPORT_RESULT_SUCCESS,
});

const reportResultFailure = () => ({
  type: actionTypes.REPORT_RESULT_FAILURE,
});

const reportResultLoading = () => ({
  type: actionTypes.REPORT_RESULT_LOADING,
});

const rankingRequest = userToken => ({
  type: actionTypes.RANKING_REQUEST,
  userToken,
});

const rankingSuccess = () => ({
  type: actionTypes.RANKING_SUCCESS,
});

const rankingFailure = () => ({
  type: actionTypes.RANKING_FAILURE,
});

const rankingLoading = () => ({
  type: actionTypes.RANKING_LOADING,
});


export default {
  setUser,
  removeUser,
  getTournamentRequest,
  getTournamentFailure,
  getTournamentSuccess,
  getTournamentLoading,
  setFilters,
  removeFilters,
  leaveTournamentRequest,
  leaveTournamentLoading,
  leaveTournamentFailure,
  leaveTournamentSuccess,
  getUserRequest,
  getUserFailure,
  getUserSuccess,
  getUserLoading,
  joinTournamentFailure,
  joinTournamentSuccess,
  joinTournamentRequest,
  joinTournamentLoading,
  getTournamentDetails,
  getTournamentDetailsLoading,
  activeGameFailure,
  activeGameLoading,
  activeGameRequest,
  activeGameSuccess,
  reportResultRequest,
  reportResultSuccess,
  reportResultFailure,
  reportResultLoading,
  gameHistoryFailure,
  gameHistoryLoading,
  gameHistoryRequest,
  gameHistorySuccess,
  rankingFailure,
  rankingLoading,
  rankingRequest,
  rankingSuccess,
};
